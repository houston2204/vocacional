<?php
$xml=new DomDocument('1.0','UTF-8');

	$invoice=$xml->createElement('Invoice');
	$invoice->setAttribute('xmlns','urn:oasis:names:specification:ubl:schema:xsd:Invoice-2');
	$invoice->setAttribute('xmlns:sac','urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1');
	$invoice->setAttribute('xmlns:cac','urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2');
	$invoice->setAttribute('xmlns:cbc','urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2');
	$invoice->setAttribute('xmlns:udt','urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2');
	$invoice->setAttribute('xmlns:ccts','urn:un:unece:uncefact:documentation:2');
	$invoice->setAttribute('xmlns:ext','urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2');
	$invoice->setAttribute('xmlns:qdt','urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2');
	$invoice->setAttribute('xmlns:ds','http://www.w3.org/2000/09/xmldsig#');
	$invoice->setAttribute('xmlns:schemaLocation','urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-2.0.xsd');
	$invoice->setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
	$xml->appendchild($invoice);
  	$ublextensions=$xml->createElement('ext:UBLExtensions'); 
  	$invoice->appendChild($ublextensions);
	
	$ublextension=$xml->createElement('ext:UBLExtension');
	$ublextensions->appendChild($ublextension);  
/*----Firma digital--------*/
	$extensioncontent=$xml->createElement('ext:ExtensionContent');
	$ublextension->appendChild($extensioncontent);	
	/*Id de la firma*/
	$signature=$xml->createElement('ds:Signature');
	$signature->setAttribute('Id','#SBK97-4573');
	$extensioncontent->appendchild($signature);
	/*Informacion sobre el valor de la firma y datos a firmar*/
	$signedinfo=$xml->createElement('ds:SignedInfo');
	$signature->appendChild($signedinfo);
	/*como se debe transformar a  forma canonica el elemento*/
	$canonicalizationmethod=$xml->createElement('ds:CanonicalizationMethod');
	$canonicalizationmethod->setAttribute('Algorithm','http://www.w3.org/TR/2001/REC-xml-c14n-20010315');
	$signedinfo->appendchild($canonicalizationmethod);
	/*Indica que tipo de algoritmo de firma se utilizara para obtener la firma*/
	$signaturemethod=$xml->createElement('ds:SignatureMethod');
	$signaturemethod->setAttribute('Algorithm','http://www.w3.org/2000/09/xmldsig#rsa-sha1');
	$signedinfo->appendchild($signaturemethod);
	/*Identifica el objeto de datos que se va a firmar */
	$reference=$xml->createElement('ds:Reference');
	$reference->setAttribute('URI',' ');
	$signedinfo->appendchild($reference);
	/*Indica un paso realizado en el procesamiento del calculo hash*/
	$transforms=$xml->createElement('ds:Transforms');
	$reference->appendchild($transforms);
	$transform=$xml->createElement('ds:Transform');
	$transform->setAttribute('Algorithm','http://www.w3.org/2000/09/xmldsig#enveloped-signature');
	$transforms->appendchild($transform);	
	/*Define la funcion hash utilizada*/
	$digestmethod=$xml->createElement('ds:DigestMethod');
	$digestmethod->setAttribute('Algorithm','http://www.w3.org/2000/09/xmldsig#sha1');
	$reference->appendchild($digestmethod);
	/*Es el valor de hash codificado en base64*/
	$digestvalue=$xml->createElement('ds:DigestValue','0tesNgtCZp+ZXKPOV1XZmuKnJF4=');
	$reference->appendchild($digestvalue);
	/*Contiene la firma en base 64*/
	$signaturevalue=$xml->createElement('ds:SignatureValue','OIFjC6jiasm/+Ojm/7DWuy1rMprVgqlii2dgytyRCLaQkupPH4ztwq5PbzEypg/S0MrBCwpyyjiC
	DpgX7kdus2hsRnfFd/HYqNsklofIltkOU+eTugLBX7Lbf3u5dY1PhnOPwNO+HlJtCGf8CTUJfZ2Q
	z+O1B6HwlZIfHdy3KGSGxlat3bjwDaWJUk1x8zCXu42CjCLA5mNpLgaVxyCqJ1xmRrGybNF14MKM
	O3F6ZJzg2s0frnvyICiI3UkKHNSwBl6rlmLIlCVlKRHoxdjoDcuzjnWTspwazscd5QoqSFaFGhV5
	gwW06IiPIdK5MxJP5HJQ2/PH7yobFbNeMv4GTw==');
	$signature->appendchild($signaturevalue);
	/*Estructura que contiene informacion del certificado firmante*/
	$keyinfo=$xml->createElement('ds:KeyInfo');
	$signature->appendchild($keyinfo);
	$x509data=$xml->createElement('ds:X509Data');
	$keyinfo->appendchild($x509data);
	$x509certificate=$xml->createElement('ds:X509Certificate','MIIGQDCCBSigAwIBAgIQHA+qvZS3O7Q8ONFCB7h7sDANBgkqhkiG9w0BAQsFADCBmzELMAkGA1UE
	BhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4GA1UEBxMHU2FsZm9yZDEaMBgG
	A1UEChMRQ09NT0RPIENBIExpbWl0ZWQxQTA/BgNVBAMTOENPTU9ETyBTSEEtMjU2IENsaWVudCBB	
	dXRoZW50aWNhdGlvbiBhbmQgU2VjdXJlIEVtYWlsIENBMB4XDTE2MDEwNTAwMDAwMFoXDTE3MDEw
	NDIzNTk1OVowggE0MQswCQYDVQQGEwJQRTEQMA4GA1UEERMHTElNQSAyNzENMAsGA1UECBMETElN
	QTETMBEGA1UEBxMKU0FOIElTSURSTzEkMCIGA1UECRMbQVYuIEpBVklFUiBQUkFETyBPRVNURSAx
	NjUwMRQwEgYDVQQKEwtERUxPU0kgUy5BLjEUMBIGA1UECxMLMjAxMDAxMjMzMzAxMTAvBgNVBAsT
	KElzc3VlZCB0aHJvdWdoIERFTE9TSSBTLkEuIEUtUEtJIE1hbmFnZXIxHzAdBgNVBAsTFkNvcnBv
	cmF0ZSBTZWN1cmUgRW1haWwxHjAcBgNVBAMTFUFsZnJlZG8gTm92b2EgTGluYXJlczEpMCcGCSqG
	SIb3DQEJARYaYW5vdm9hQGZyYW5xdWljaWFzcGVydS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IB
	DwAwggEKAoIBAQClCmETAt24JTvdH0aAlQBnEu7CSMEwKmUTljKTcUD945WLW3Vm+x8GQZespkqh
	TRl1+MbzEV/d0pga04sUNMHDz5rhxrw3JHfT4UXKOqyBQYdq3kIVGz5hVHU9Bat+FClA3lym1OYz
	7UlKLfyNTUyEivddHBiQ4AOYpAnrH36AL5B6CPQB7GI6tVkFRTg4uXRWhVvQAv0vTu9BGTGbOtIy
	VJf+ikzR4vAPi9qh2nGHC2kd65Otw+k/yGiKkLg3v28Dqe/jU/47U7FM7J7iK/nJNrMTMf2PK4OP
	6rduXLmFWKKRWe2n8nJUCvIVC7HDArAxBhNgeBpNLW6kYdfg7Nn1AgMBAAGjggHiMIIB3jAfBgNV
	HSMEGDAWgBSSYWuC4aKgqk/sZ/HCo/e0gADB7DAdBgNVHQ4EFgQUhMcEh7bTw9QWG9mniQUykUwh
	tNUwDgYDVR0PAQH/BAQDAgWgMAwGA1UdEwEB/wQCMAAwHQYDVR0lBBYwFAYIKwYBBQUHAwQGCCsG
	AQUFBwMCMEYGA1UdIAQ/MD0wOwYMKwYBBAGyMQECAQMFMCswKQYIKwYBBQUHAgEWHWh0dHBzOi8v
	c2VjdXJlLmNvbW9kby5uZXQvQ1BTMF0GA1UdHwRWMFQwUqBQoE6GTGh0dHA6Ly9jcmwuY29tb2Rv
	Y2EuY29tL0NPTU9ET1NIQTI1NkNsaWVudEF1dGhlbnRpY2F0aW9uYW5kU2VjdXJlRW1haWxDQS5j
	cmwwgZAGCCsGAQUFBwEBBIGDMIGAMFgGCCsGAQUFBzAChkxodHRwOi8vY3J0LmNvbW9kb2NhLmNv
	bS9DT01PRE9TSEEyNTZDbGllbnRBdXRoZW50aWNhdGlvbmFuZFNlY3VyZUVtYWlsQ0EuY3J0MCQG
	CCsGAQUFBzABhhhodHRwOi8vb2NzcC5jb21vZG9jYS5jb20wJQYDVR0RBB4wHIEaYW5vdm9hQGZy
	YW5xdWljaWFzcGVydS5jb20wDQYJKoZIhvcNAQELBQADggEBABSBRgkDVDhVDq811csfNaSWrts+
	cYCGHh2rAxO/EdvSi/sl4c8wXfQ1k1Llg2GPMVJnVnM7iEdWqtrCvgiKLIhm/1sCgqEGWlaCuSCy
	mLXaGGUpYyd/y4zbjXwBTUGb4XnynVrJU2+whE7v/QbFtHaaQpT1yZGjPLBpjynBV8buUc+yO9Ot
	aJPk3AxgALxUUiO0+sN9/S6OwFxiTWjzSkmrdcNVICDlHEywSOn8VfJAWCphOjo+ESB7RdWqjVEa
	wAVQdu2V2SGwayhGDizEh7KBzdz49kzR2rDRIsLSYcI3gJrqG/uuNpE278kTCaddc2k+coD3UF/8
	DoviiwJCFA8=');
	$x509data->appendchild($x509certificate);
/*----fin de la firma digital*/
/*Componente de extension para especificar informacion adicional*/
	$ublextension=$xml->createElement('ext:UBLExtension');
	$ublextensions->appendchild($ublextension);
	$extensioncontent=$xml->createElement('ext:ExtensionContent');
	$ublextension->appendchild($extensioncontent);
	/*Informacion adicional recomendado por sunat*/
	$additionalinformation=$xml->createElement('sac:AdditionalInformation');
	$extensioncontent->appendchild($additionalinformation);
	/*Informacion adicional de tipo monetario*/
	$additionalmonetarytotal=$xml->createElement('sac:AdditionalMonetaryTotal');
	$additionalinformation->appendchild($additionalmonetarytotal);
	/*Codigo del concepto adicional*/
	$id=$xml->createElement('cbc:ID','1001');
	$additionalmonetarytotal->appendchild($id);
	/*nombre del concepto*/
	$name=$xml->createElement('cbc:Name');
	$additionalmonetarytotal->appendchild($name);
	/*Monto de referencia*/
	$refereceamount=$xml->createElement('cbc:ReferenceAmount');
	$additionalmonetarytotal->appendchild($refereceamount);
	/*Monto a pagar*/
	$payableamount=$xml->createElement('cbc:PayableAmount','11.92');
	$payableamount->setAttribute('currencyID','PEN');
	$additionalmonetarytotal->appendchild($payableamount);
	/*porcentaje */
	$percent=$xml->createElement('cbc:Percent');
	$additionalmonetarytotal->appendchild($percent);

	/*Monto Total*/
	$totalamount=$xml->createElement('sac:TotalAmount');
	$additionalmonetarytotal->appendchild($totalamount);
	/**/

	/*Informacion adicional de cualquier tipo*/
	$additionalproperty=$xml->createElement('sac:AdditionalProperty');
	$additionalinformation->appendchild($additionalproperty);
	/*Id*/
	$id=$xml->createElement('cbc:ID','1000');
	$additionalproperty->appendchild($id);
	/*cantidad*/
	$value=$xml->createElement('cbc:Value','&lt;![CDATA[Son: Catorce con 90/100  Nuevos Soles]]&gt;');
	$additionalproperty->appendchild($value);
/*fin de la informacion adicional*/
/*Version del ubl*/
	$ubl=$xml->createElement('cbc:UBLVersionID','2.0');
	$invoice->appendchild($ubl);
/*Versiond el a estructura del documento*/
	$customizationid=$xml->createElement('cbc:CustomizationID','1.0');
	$invoice->appendchild($customizationid);
/*Numero unico asignado al documento por el emisor (Serie + numero correlativo factura)*/
	$id=$xml->createElement('cbc:ID','BK97-45273');
	$invoice->appendchild($id);
/*Fecha de emision del documento(yyyy-mm--dd)*/
	$issuedate=$xml->createElement('cbc:IssueDate','2016-04-23');
	$invoice->appendchild($issuedate);
/*Codigo del tipo de documento factura, boleta etc*/
	$invoicetypecode=$xml->createElement('cbc:InvoiceTypeCode');
	$invoice->appendchild($invoicetypecode);
/*Tipo de moneda en la cual se emite el documento*/
	$documentcurrencycode=$xml->createElement('cbc:DocumentCurrencyCode');
	$invoice->appendchild($documentcurrencycode);
/*Guias de remision*/
	$despatchdocumentreference=$xml->createElement('cbc:DespatchDocumentReference');
	$invoice->appendchild($despatchdocumentreference);
	/*Numero de documento*/
	$id=$xml->createElement('cbc:ID');
	$despatchdocumentreference->appendchild($id);
	/*Codigo del tipo de docuemtno de referencia*/
	$documenttypecode=$xml->createElement('cbc:DocumentTypeCode');
	$despatchdocumentreference->appendchild($documenttypecode);
/*Cualquier otro documento relacionado con la operacion*/
	$additionaldocumentreference=$xml->createElement('cbc:AdditionalDocumentReference');
	$invoice->appendchild($additionaldocumentreference);
	/*Numero de documento*/
	$id=$xml->createElement('cbc:ID');
	$additionaldocumentreference->appendchild($id);
	/*codigo del documento adicional*/
	$documenttypecode=$xml->createElement('cbc:DocumentTypeCode');
	$additionaldocumentreference->appendchild($documenttypecode);
/*Referencia a la firma digital*/
	$signature=$xml->createElement('cac:Signature');
	$invoice->appendchild($signature);
	/*Identificador de la firma*/
	$id=$xml->createElement('cbc:ID','SBK97-45273');
	$signature->appendchild($id);
	/**/
	$signatoryparty=$xml->createElement('cac:SignatoryParty');
	$signature->appendchild($signatoryparty);
	/*Parte firmante*/
	$partyidentification=$xml->createElement('cac:PartyIdentification');
	$signatoryparty->appendchild($partyidentification);
	/*Identificacion de la parte firmante*/
	$id=$xml->createElement('cbc:ID','20100123330');
	$partyidentification->appendchild($id);
	/**/
	$partyname=$xml->createElement('cac:PartyName');
	$signatoryparty->appendchild($partyname);
	/*Nombre de la parte firmante*/
	$name=$xml->createElement('cbc:Name','<![CDATA[SUNAT]]>');
	$partyname->appendchild($name);
/*Asociacion con la firma codificada*/
	$digitalsignatureattachment=$xml->createElement('cac:DigitalSignatureAttachmen');
	$signature->appendchild($digitalsignatureattachment);
	/*Referencia a un documento vinculado pueden ser externos o internos*/
	$externalreference=$xml->createElement('cac:ExternalReference');
	$digitalsignatureattachment->appendchild($externalreference);
	/*url que identifica la localizacion de la ifrma*/
	$uri=$xml->createElement('cbc:URI','#SBK97-45273');
	$digitalsignatureattachment->appendchild($uri);
/*Fin asociacion con firma codificada*/
/*Datos del emisor del documento*/
	$accountingsupplierparty=$xml->createElement('cac:AccountingSupplierParty');
	$invoice->appendchild($accountingsupplierparty);
	/*Numero de documento de indentidad (RUC)*/
	$customerassignedaccountid=$xml->createElement('cbc:CustomerAssignedAccountID','20100123330');
	$accountingsupplierparty->appendchild($customerassignedaccountid);
	/*Tipo de documento de identificacion*/
	$additionalaccountid=$xml->createElement('cbc:AdditionalAccountID');
	$accountingsupplierparty->appendchild($additionalaccountid);
	/**/
	$party=$xml->createElement('cac:Party');
	$accountingsupplierparty->appendchild($party);
	/**/
	$partyname=$xml->createElement('cac:PartyName');
	$party->appendchild($partyname);
	/*Nombre Comercial*/
	$name=$xml->createElement('cbc:Name','<![CDATA[KFC]]>');
	$partyname->appendchild($name);
	/*Domicilio Fiscal*/
	$postaladdres=$xml->createElement('cac:PostalAddres');
	$party->appendchild($postaladdres);
	/*Codigo de ubigeo*/
	$id=$xml->createElement('cbc:ID','150131');
	$postaladdres->appendchild($id);
	/*Direccion completa y detallada*/
	$streetname=$xml->createElement('cbc:StreetName','<![CDATA[Av. Javier Prado Oeste 1650]]>');
	$postaladdres->appendchild($streetname);
	/*Urbanizacion o zona*/
	$citysubdivisionname=$xml->createElement('cbc:CitySubdivisionName');
	$postaladdres->appendchild($citysubdivisionname);
	/*Departamento*/
	$cityname=$xml->createElement('cbc:CityName','Huanuco');
	$postaladdres->appendchild($cityname);
	/*Provincia*/
	$countrysubentity=$xml->createElement('cbc:CountrySubEntity','Huanuco');
	$postaladdres->appendchild($countrysubentity);
	/*Distrito*/
	$district=$xml->createElement('cbc:District','Huanuco');
	$postaladdres->appendchild($district);
	/*Codigo del pais*/
	$country=$xml->createElement('cac:Country');
	$postaladdres->appendchild($country);
	$identificationcode=$xml->createElement('cbc:IdentificationCode','PE');
	$country->appendchild($identificationcode);
	/**/
	$partylegalentity=$xml->createElement('cac:PartyLegalEntity');
	$party->appendchild($partylegalentity);
	/*Apellidos y nombres o denominacion o razon social*/
	$registrationname=$xml->createElement('cbc:RegistrationName','<![CDATA[DELOSI S.A.]]>');
	$partylegalentity->appendchild($registrationname);
/*Datos del adquiriente o usuario*/
	$accountingcustomerparty=$xml->createElement('cac:AccountingCostumerParty');
	$invoice->appendchild($accountingcustomerparty);
	/*Numero de documento de identidad (DNI,RUC)*/
	$customerassignedaccountid=$xml->createElement('cbc:CustomerAssignedAccountID','0');
	$accountingcustomerparty->appendchild($customerassignedaccountid);
	/*Tipo de documento de identificacion*/
	$additionalaccountid=$xml->createElement('cbc:AdditionalAccountID','0');
	$accountingcustomerparty->appendchild($additionalaccountid);
	/**/
	$party=$xml->createElement('cac:Party');
	$accountingcustomerparty->appendchild($party);
	/**/
	$partylegalentity=$xml->createElement('cac:PartyLegalEntity');
	$party->appendchild($partylegalentity);
	/*Apellidos y nombres o denominacion o razon social segun ruc*/
	$registrationname=$xml->createElement('cbc:RegistrationName','Meliza');
/*Impuestos globales*/
	$taxtotal=$xml->createElement('cac:TaxTotal');
	$invoice->appendchild($taxtotal);
	/*Importe total de unt ributo para la factura*/
	$taxamount=$xml->createElement('cbcTaxAmount');
	$taxamount->setAttribute('currencyID','PEN');
	$taxtotal->appendchild($taxamount);
	/**/
	$taxsubtotal=$xml->createElement('cac:TaxSubtotal');
	$taxtotal->appendchild($taxsubtotal);
	/*Importe explícito  a tributar*/
	$taxamount=$xml->createElement('cbc:TaxAmount');
	$taxamount->setAttribute('currencyID','PEN');
	$taxsubtotal->appendchild($taxamount);
	/*Identificacion del tributo segun catalogo 05*/
	$taxcategory=$xml->createElement('cac:TaxCategory');
	$taxsubtotal->appendchild($taxcategory);
	$taxscheme=$xml->createElement('cac:TaxScheme');
	$taxcategory->appendchild($taxscheme);
	/*ID del tributo*/
	$id=$xml->createElement('cbc:ID','1000');
	$taxscheme->appendchild($id);
	/*Nombre del tributo*/
	$name=$xml->createElement('cbc:Name','IGV');
	$taxscheme->appendchild($name);
	/*Codigo del tributo */
	$taxtypecode=$xml->createElement('cbc:TaxTypeCode','VAT');
	$taxscheme->appendchild($taxtypecode);
/*Totales  a pagar de la factura y cargos*/
	$legalmonetarytotal=$xml->createElement('cac:LegalMonetaryTotal');
	$invoice->appendchild($legalmonetarytotal);
	/*Importe total de cargos aplicados al total de la factura*/
	$chargetotalamount=$xml->createElement('cbc:ChargeTotalAmount','0.83');
	$chargetotalamount->setAttribute('currencyID','PEN');
	$legalmonetarytotal->appendchild($chargetotalamount);
	/*Moneda e importe total a pagar*/
	$payableamount=$xml->createElement('cbc:PayableAmount','14.9');
	$payableamount->setAttribute('CurrencyID','PEN');
	$legalmonetarytotal->appendchild($payableamount);
/*Items de la factura*/
	$invoiceline=$xml->createElement('cac:InvoiceLine');
	$invoice->appendchild($invoiceline);
	/*numero del orden del item*/
	$id=$xml->createElement('cbc:ID','1');
	$invoiceline->appendchild($id);
	/*Unidad de medida por item y cantidad*/
	$invoicequantity=$xml->createElement('cbc:InvoiceQuantity','1');
	$invoicequantity->setAttribute('unitCode','NIU');
	$invoiceline->appendchild($invoicequantity);
	/*Moneda e importe monetario quie es el total de la linea de detalle sin igv incluye descuentoos*/
	$lineextensionamount=$xml->createElement('cbc:LineExtensionAmount','11.92');
	$lineextensionamount->setAttribute('currencyID','PEN');
	$invoiceline->appendchild($lineextensionamount);
	/*Valores unitarios*/
	$pricingreference=$xml->createElement('cac:PricingReference');
	$invoiceline->appendchild($pricingreference);
	/**/
	$alternativeconditionprice=$xml->createElement('cac:AlternativeConditionPrice');
	$pricingreference->appendchild($alternativeconditionprice);
	/*Monto del valor unitario*/
	$priceamount=$xml->createElement('cbc:PriceAmount','14.9');
	$priceamount->setAttribute('currencyID','PEN');
	$alternativeconditionprice->appendchild($priceamount);
	/*Codigo del valor unitario*/
	$pricetypecode=$xml->createElement('cbc:PriceTypeCode','01');
	$alternativeconditionprice->appendchild($pricetypecode);
	/*Informacion acerca del importe total de un tipo particular de impuesto una repeticion por igv, isc*/
	$taxtotal=$xml->createElement('cac:TaxTotal');
	$invoiceline->appendchild($taxtotal);
	/*Importe total de un tributo para este item*/
	$taxamount=$xml->createElement('cbc:TaxAmount','2.15');
	$taxamount->setAttribute('currencyID','2.15');
	/**/
	$taxsubtotal=$xml->createElement('cac:TaxSubtotal');
	$taxtotal->appendchild($taxsubtotal);
	/*importe explicito a tributar = tasa porcentaje * base imponible*/
	$taxamount=$xml->createElement('cbc:TaxAmount','2.15');
	$taxamount->setAttribute('currencyID','PEN');
	$taxsubtotal->appendchild($taxamount);
	/*afectacion del igv catalogo 7*/
	$taxcategory=$xml->createElement('cac:TaxCategory');
	$taxsubtotal->appendchild($taxcategory);
	$taxexemptionreasoncode=$xml->createElement('cbc:TaxExemptionReasonCode','10');
	$taxcategory->appendchild($taxexemptionreasoncode);
	/*sistema de isc*/
	$tierrange=$xml->createElement('cbc:TierRange');
	$taxcategory->appendchild($tierrange);
	/*identificacion del tributo segun catalogo 05*/
	$taxscheme=$xml->createElement('cac:TaxScheme');
	$taxcategory->appendchild($taxscheme);
	/*id*/
	$id=$xml->createElement('cbc:ID','1000');
	$taxscheme->appendchild($id);
	/*nombre del tributo*/
	$name=$xml->createElement('cbc:Name','IGV');
	$taxscheme->appendchild($name);
	/*Codigo del tributo */
	$taxtypecode=$xml->createElement('cbc:TaxTypeCode','VAT');
	$taxscheme->appendchild($taxtypecode);
	/**/
	$item=$xml->createElement('cac:Item');
	$invoiceline->appendchild($item);
	/*Descripcion detallada del bien vendido o cedido en uso*/
	$description=$xml->createElement('cbc:Description','<![CDATA[(P) CMB CLASSIC]]>');
	$item->appendchild($description);
	/*Identificador de elemetnos del item*/
	$sellersitemidentification=$xml->createElement('cac:SellersItemIdentication');
	$item->appendchild($sellersitemidentification);
	/*codigo del producto*/
	$id=$xml->createElement('cbc:ID','4023171');
	$sellersitemidentification->appendchild($id);
	/*Price*/
	$price=$xml->createElement('cac:Price');
	$invoiceline->appendchild($price);
	/*valores de venta unitarios por item no incluye impuestops*/
	$priceamount=$xml->createElement('cbc:Priceamount','11.92');
	$priceamount->setAttribute('CurrencyID','PEN');
	$price->appendchild($priceamount);





$xml->formatOutput=true;
$strings_xml=$xml->saveXML();
$xml->save('D:/prueba.xml');