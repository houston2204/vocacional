
@extends('nuevo.index')
@section('titulo')
	Stock
@endsection
@section('estilos')
 	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
@endsection
@section('alertas')
	<div id="notificaciones_result"></div>
@endsection
@section('contenido')	
	<div class="panel panel-default">
	@include('producto_tienda.partials.modal-producto	')
	<div class="panel-body">
		<h4>Stock</h4>
			<div class="box-body">
				<table border="0"  id="example1" class="table table table-hover">	 								          				
					<thead>
						<tr>						
							<th>Tienda</th>							
							<th>Producto</th>
							<th>Cantidad</th>							
							<th>Acciones</th>
						</tr>
					</thead>													
				</table>
			</div>
		</div>
	</div>
	<input type="hidden" name="_token" value="{{csrf_token()}}" id="token"> 
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>
<script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js" ></script>
<script type="text/javascript">

			/*Pasar los datos al modal*/
	function aumentar(id){
		           
	   	$("#nombreprod").text('Agregar a tienda');
		$("#mid_tiendaprod").val(id);				
		$('#modal').modal('show');
	    $('#crear').val('1');
		}
		/*Carga todos los productos y los agrega a la tabla*/
	$(document).ready(function() {			
		
		/*LLenar los datos a la tabla*/
 		$('#example1').DataTable( {
	        "processing": true,
	        "serverSide": true,  
	        'bFilter':true,    
	        "columnDefs": [
	            {
	                "targets": [ 3 ],
	                "visible": true,
	                "searchable": false
	            },
	        
	           
	        ]  , "oLanguage": {
		           		"oPaginate": {
		 					"sNext": "Siguiente",
		 					"sPrevious": "Anterior",				 			
		           		},
		            	"sSearch": "Buscar"	,
		            	"sInfo": " Mostrando _START_ a _END_ de _TOTAL_ entidades",
		            	"sLengthMenu": "Mostrar _MENU_ resultados por página",
		            	"sInfoFiltered": " - filtrando de _MAX_ resultados"
		         	},
	        "ajax": "./api/tiendaprod",                 
	        'columns':[
	      		
	        	{data: 'tienda'},      
	        	{data: 'producto'},
	        	{data: 'cantidad'},
	        	{data: 'action'},     	
	        ]
    	});

	  	/*Para el registro de nuevo producto o edicion*/
		
	  	$('#btn-addagregar').click(function(){
	  	
	  		var form=$('#form-tiendaprod');
	  		var divresul = "notificaciones_result";
	  		var url=form.attr('action');
	  		var token=$("#token").val();

	  		var dataString = new FormData(document.getElementById("form-tiendaprod"));	  		  		  
	  		$.ajax({
	  		  	headers:{'X-CSRF-TOKEN':token},
	            type: "POST",
	            url: url,
	            data: dataString,
	            cache: false,
					contentType: false,
				processData: false,

	            success: function(result) {	   
	            	
	      	      	$('#form-tiendaprod')[0].reset();
	             	$('#example1').DataTable().ajax.reload();	
	             	$("#"+divresul+"").html(result);		             		          
	            }

	        });  		  	
  		});
	}); 

	/*Para borrar el producto*/
		
		
	</script>

@endsection

	
	