 <div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="nombreprod" class="modal-title" id="gridModalLabel"></h4>
            </div>
            {!!Form::open(['route'=>'tienda_productos.store','method'=>'POST','id'=>'form-tiendaprod'])!!}
                <div class="modal-body">
              
            <!--hidden para el id de la tienda cuando se actualiza-->
                <input type="hidden" id='mid_tiendaprod' value="1" name="mid_tiendaprod">
                    <div class="container-fluid bd-example-row">
                        <div class="row">                   
                          <input type="hidden" id='tipo' value="1" name="tipo">
                            <div class="form-group col-xs-11 col-margins">
                                <div class="inner-addon right-addon">                    
                                    <select name="tienda" id="mtienda" class="form-control">
                                  
                                        @if(Auth::user()->nivel<'3')
                                        <option value="{{$tiendas->id}}">{{$tiendas->nombre_comercial}}</option>
                                      
                                        @else
                                        @foreach($tiendas as $tienda)
                                        <option value="{{$tienda->id}}">{{$tienda->nombre_comercial}}</option>
                                             @endforeach
                                          @endif
                                  
                                    
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group col-xs-6 col-margins" >                  
                                <div class="inner-addon left-addon">                                    
                                    
                                    {!!Form::text('cantidad', null,array('class'=>'form-control','id'=>'mcantidad','placeholder'=>'Cantidad'))!!}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btn-addagregar"  class="btn btn-primary" data-dismiss="modal">Guardar</button>
                </div>
            {!!Form::close()!!}
        </div>
    </div>
</div> 
 
