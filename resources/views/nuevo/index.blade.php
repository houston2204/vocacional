<!DOCTYPE html>
<html lang="en">
  	<head>
    	<meta charset="utf-8">
    	<title>@yield('titulo')</title> <!-- Esta seccion corresponde al titulo -->
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>   
    	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>  
        <link href="https://fonts.googleapis.com/css?family=Roboto:900" rel="stylesheet"> 
    	<link href="<?=URL::to('css/style.css'); ?>" 	rel="stylesheet">   
    	<link href="<?=URL::to('bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"> 	     
    	<script src="<?=URL::to('dist/sweetalert-dev.js'); ?>"></script>
  		<link href="<?=URL::to('dist/sweetalert.css'); ?>"  rel="stylesheet">
    	
    	@yield('estilos')
  	</head>
  	<body>    	
    	<div class="container-fluid">
    		<div class="row sinpadding">
    			<div class="col-xs-12 col-sm-1  col-md-2" id="tienda">
    				<div class="tienda">
						<a href="/" style="color: black"> <p class="nombre-tienda">IMAGINATIC'S</p> <i class="material-icons">home</i></a>
    				</div>
    			</div>
    			<div class="col-xs-12 col-sm-11 col-md-10" id="notificaciones">
    				<div class="notificaciones">
    					<div class="row">  
    						<div class="col-xs-3 col-md-5 col-sm-3">
		    					
	    					</div>
	    					<div class="col-xs-9 col-md-6 col-sm-6 ">		    				
								<ul class="nav navbar-nav navbar-right">
									<li><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true" style="color: white"></span>  <span style="color: white">UNIVERSIDAD NACIONAL HERMILIO VALDIZAN</span></a>
									</li>															    
							    </ul>
	    					</div>	    					
	    				</div>
    				</div>    				
    			</div>
    		</div>
            
   	    	<div class="row" >			  	
			  	<div class="col-xs-12 col-sm-12" id="contenido" >
			  		<div class="contenido"  >
			  			<div class="alertas">			  			
			  				@yield('alertas')
			  			</div>
			  			<div class="formularios" >                        
			  			    @include('nuevo.partials.success-noajax')
			  				@yield('contenido')
			  			</div>
			  		</div>
			  	</div>			
			</div>
		</div>			
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    	<script src="<?=URL::to('js/main.js'); ?>"></script>
   
    	<script src="<?=URl::to('js/bootstrap.min.js');?>"</script>    	
    	@yield('script')
  	</body>
</html>