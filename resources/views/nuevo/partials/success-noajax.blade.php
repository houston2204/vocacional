 
@if(Session::has('message'))
<div class="alert alert-danger alert-dismissable">

     <button type="button" class="close" data-dismiss="alert">&times;</button>
 		 <strong>¡Lo lamento! </strong> {{Session::get('message')}}
  
</div>
 
 @elseif($errors->any())
<div class="alert alert-danger alert-dismissable">

     <button type="button" class="close" data-dismiss="alert">&times;</button>
 		

 		 @foreach($errors->all() as $message)
 		  <strong>¡Lo lamento! </strong> {{$message}}<br>
 		 @endforeach
  
</div>
  @endif