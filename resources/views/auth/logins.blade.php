<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <title>Sistema de facturación electrónica</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/materialize.css">
    <body>
        <style>
            .row{
                height: 97vh;                
            }
            .col h2{
                font-family: 'Lato';
                font-weight: bold;
                font-size: 2.3em;
            }
            .col p{
                font-size: 1.35em;
            }

            img{
                width: 100%;
            }
            
            .container{
              /*
                background: green;*/
            }
                    
            .login{                    
                margin-top: 60px;          
            }
            .login #login{
                margin: 0 auto;

            }
            form{
                background-color:#F1BC2A;
                padding: 40px;
                width: 70%;
                border-radius: 15px;
            }
            form > img{
                width: 40%;
            }
            body{
                /*background: gray;*/
                background: #00AFEF;
                /*background: linear-gradient(75deg, #00AFEF, #F1BC2A);*/
                /*background: linear-gradient(, #008080, #ffebcd);*/
            }
        </style>
        <div class="container">
            <div class="row">                
                <div class="col s12 m6 l6">
                    <div class="contenido descripcion center">                    
                        <div class="valign">                            
                            <h2 class="">IMAGINATIC'S</h2>
                            <p class="">Desarrollando la tecnología que facilita tu vida.</p>
                            <img src="img/responsive.png" alt="Logo" >                      
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l6">
                    <div class="contenido login center-align">                        
                        <div class="valign">
                             <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo electronico</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Entrar
                                </button>

                                
                            </div>
                       
                    </form>
                        <a class="btn btn-primary" href="{{ url('/register') }}">Registrarme</a>
                         </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    
</html>



