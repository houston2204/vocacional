<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <title>Sistema de facturación electrónica</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="slide/superslides.css">
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>
    <body>


     <nav class="navbar navbar-default shadow" role="navigation">
        <div class="container">
            <div class="row content-nav">
                <div class="col-md-10">Imaginatic's</div>

                <div class="col-md-2">
                    <button class="btn btn-material" data-toggle="modal" data-target="#modalLogin">Ingresar</button>
                </div>
                
            </div>
        </div>
    </nav>

    <!--<section class="services-section" >
        <div class="container">
            <div class="row">                
                <div class="col-lg-6 col-md-6">
                    <img class="img-resdponsive" src="img/responsive.png" alt="Logo" >
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="slogan">                    
                            <p class="">Desarrollando la tecnología que facilita tu vida.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <div id="slides">
        <nav class="slides-navigation">
            <a href="#" class="next">&#62;</a>
            <a href="#" class="prev">&#60;</a>
        </nav>
        <ul class="slides-container">
            <li>
                <img class="imgSlider" src="img/slide/img1.jpeg" alt="">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4 slogans1">
                        <h3>Desarrollando la tecnologia que facilita tu vida.</h3>    
                    </div>
                    <div class="col-lg-4">
                    </div>
                </div>
            </li>
            <li>
                <img class="imgSlider" src="img/slide/img2.jpeg" alt="">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4 slogans1">
                        <h3>Desarrollando la tecnologia que facilita tu vida.</h3>    
                    </div>
                    <div class="col-lg-4">
                    </div>
                </div>
                </li>
            <li>
                <img class="imgSlider" src="img/slide/img3.jpg" alt="">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4 slogans1">
                        <h3>Desarrollando la tecnologia que facilita tu vida.</h3>    
                    </div>
                    <div class="col-lg-4">
                    </div>
                </div>
            </li>
        </ul>

    </div>


    <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="ModalLogin">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Login</h4>
          </div>
          <form role="form" action="{{route('doc_electronicos.index')}}" id="login">
          <div class="modal-body">
                     <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="email" class="form-control" 
                                               placeholder="Introduce tu email">
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control"  
                                             placeholder="Contraseña">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">Recordar Contraseña
                            </label>
                        </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-material">Iniciar Sesion</button>
          </div>
          </form>     
        </div>
      </div>
    </div>


    </body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="slide/jquery.superslides.js"></script>
    <script>
    $('#slides').superslides();
    </script>
</html>

