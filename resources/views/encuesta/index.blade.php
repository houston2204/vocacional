
@extends('nuevo.index')
@section('titulo')
Test Vocacional
@endsection
@section('estilos')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans|Roboto" rel="stylesheet">
    <script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,900" rel="stylesheet"> 
@endsection
@section('contenido')	
	<div class="container" >
        <div class="row centered-form">
       
        	
	        <div class="col-xs-12 col-sm-12 col-md-12 " style="z-index: 10000">	 <br><br>       	
	        	<div class="panel panel-default" >
        			<div class="panel-heading" align="center">
		    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">CASM83</span>
		 			</div>
		 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
		 				
		 				<input type="hidden" value="pregunta">
			    		<form role="form" style="padding: 10px;" action="{{URL::to('registrar/'.$id)}}" >	
			    			<div class="row" style=" font-family: 'Roboto', sans-serif; font-size: 1.2em">
			    			
			    				<div align="center"><b>INSTRUCCIONES</b></div>
								<p>Este es un inventario de Intereses Vocacionales y Ocupacionales, en el que Ud. encontrará un conjunto de pares de preguntas, permitiéndole obtener su perfil de preferencias Profesionales y Ocupacionales. Para ello solo tiene que elegir entre dos alternativas; ejemplo:</p><br>
								<p style="padding-left: 20px; ">
									<b>A)</b> LE AGRADA LEER LIBROS SOBRE FÍSICA NUCLEAR; O<br>
									<b>B)</b> PREFIERE LEER LIBROS DE HISTORIA UNIVERSAL. <br><br>

									- Si su respuesta es "a" deberá seleccionar con un click. <br>
									- Si su respuesta es "b" deberá seleccionar con un click. <br> 
									- Si su respuesta es tanto para "a" como para "b" seleccionar ambas haciendo un click en cada una <br>
									- Si tanto "a" como "b" no le interesan, no seleccione ninguna. <br>
								</p>	
								<p>Como nota importante cabe señalar que NO HAY RESPUESTAS BUENAS NI MALAS; y si Ud. desea un resultado  confiable, procure contestar, en función de lo que a Ud. realmente le interesa y no en base a lo que otros podrían opinar (criterios ajenos a los suyos)</p>				    						    			
				    		</div>				    		
				    		<input type="hidden" name="tipo" value="{{$id}}">			    		
			    			<input type="submit" value="Iniciar" class="btn btn-succes btn-block" style="background-color: #00c853; font-family: 'Roboto', sans-serif;  font-size: 1.5em; letter-spacing: 2px; color: white"  >				    					    			 		
			    		</form>
		    		</div>
		    	</div>
	    	</div>
    	</div>
    </div>
@endsection
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script>   	
            $("#opcion1").click(function() {
  				$('#checkbox1').text('a');  				
			});	  
			$("#opcion2").click(function() {
  				$('#checkbox1').text('b');  				
			});	
	</script>
@endsection