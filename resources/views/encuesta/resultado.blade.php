
@extends('nuevo.index')
@section('titulo')
Test Vocacional
@endsection
@section('estilos')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans|Roboto" rel="stylesheet">
    <script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
@endsection
@section('contenido')	
	<div class="container" >
        <div class="row centered-form">
        <img  src="{{URL::to('img/fondo.jpg')}}" width="100%" style="position: absolute;top: 0px;left: 0px;z-index: 1; background-color:  rgba(255,255,255,0.5);opacity: 1;
			">
        	
	        <div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1" style="z-index: 10000">	 <br>
	        	<div class="panel panel-default">
        			<div class="panel-heading" align="center">
		    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11"><b>Resultados: Veracidad: {{$veracidad}}, validez: {{$validez}}</b>
		 			</div>
		 			<div class="panel-body"   style="background-color: #FFFDE7; opacity: 1; height: 430px; width: 100%"  align="center"> 
		 				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
						  <script type="text/javascript">
						    google.charts.load("current", {packages:['corechart']});
						    google.charts.setOnLoadCallback(drawChart);
						    function drawChart() {
						      var data = google.visualization.arrayToDataTable([
						        ["Element", "Density", { role: "style" } ],
						        ["CCFM", {{$ccfm}}, "#b87333"],
						        ["CCSS", {{$ccss}}, "#90CAF9"],
						        ["CCNA", {{$ccna}}, "#536DFE"],
						        ["CCCO", {{$ccco}}, "#80CBC4"],
						        ["ARTE", {{$arte}}, "#40C4FF"],
						        ["BURO", {{$buro}}, "#81C784"],
						        ["CCEP", {{$ccep}}, "#D4E157"],
						        ["HAA", {{$haa}}, "#69F0AE"],
						        ["FINA", {{$fina}}, "#B2FF59"],
						        ["LING", {{$ling}}, "#EEFF41"],
						        ["JURI", {{$juri}}, "#FFEE58"]						        						        
						      ]);

						      var view = new google.visualization.DataView(data);
						      view.setColumns([0, 1,
						                       { calc: "stringify",
						                         sourceColumn: 1,
						                         type: "string",
						                         role: "annotation" },
						                       2]);

						      var options = {
						        title: "Inventario de Intereses CAMS83 R2010",						       
						        bar: {groupWidth: "90%"},
						        legend: { position: "none" },
						      };
						      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
						      chart.draw(view, options);
						  }
						  </script>
						<div id="columnchart_values" style="width: 900px; height: 300px;"></div>
		    		</div>		    		
		    	</div>
	    	</div>
	    	<div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1" style="z-index: 10000">	 <br>
	        	<div class="panel panel-default">
        			<div class="panel-heading" align="center">
		    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11"><b>Explicación</b>
		 			</div>
		 			<div class="panel-body"   style="background-color: #FFFDE7; opacity: 1; width: 100%"  align="center">		  
		 				<div class="table-striped">
		 					<?php
		    		 	foreach ($result as $key => $row) {
			            	$aux[$key] = $row['valor'];
			            }
			            array_multisort($aux, SORT_DESC, $result);?>
							<table class="table"> 
								<tr class="info">
									<th>CODIGO</th>
									<th>NOMBRE</th>
									<th align="center">VALOR</th>
									<th>PROFESIONES Y OCUPACIONES</th>
								</tr>								
								<?php
									foreach ($result as $key => $row) { 
								?>
								<tr>
									<td class="success"><?php echo $row['codigo']; ?></td>
									<td><?php echo $row['nombre']; ?></td>
									<td align="center" class="warning"><?php echo $row['valor']; ?></td>
									<td><?php echo $row['descripcion']; ?></td>
								</tr>
								<?php } ?>
							</table>					
						</div>
		    		</div>		    		
		    	</div>
	    	</div>
    	</div>
    </div>
@endsection