
@extends('nuevo.index')
@section('titulo')
Test Vocacional
@endsection
@section('estilos')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans|Roboto" rel="stylesheet">
    <script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
@endsection
@section('contenido')	
	<div class="container" >
        <div class="row centered-form">
      
        	
	        <div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1" style="z-index: 10000">	 <br>
	        	<div class="panel panel-default">
        			<div class="panel-heading" align="center">
		    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11"><b>Resultados
		 			</div>
		 			<div class="panel-body"   style="background-color: #FFFDE7; opacity: 1; height: 430px; width: 100%"  align="center"> 
		 				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					    <script type="text/javascript">
					      google.charts.load("current", {packages:["corechart"]});
					      google.charts.setOnLoadCallback(drawChart);
					      function drawChart() {
					        var data = google.visualization.arrayToDataTable([
					          ['DESCRIPCION', 'VALOR'],
					          ['Apto',    {{$a}}],
					          ['No apto',    {{$b}}],
					        ]);

					        var options = {
					          title: 'Lista de aptitudes',
					          is3D: true,
					        };

					        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
					        chart.draw(data, options);
					      }
					    </script>			
						<div id="piechart_3d" style="width: 900px; height: 300px;"></div>
		    		</div>		    		
		    	</div>
	    	</div>
	    	<div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1" style="z-index: 10000">	 <br>
	        	<div class="panel panel-default">
        			<div class="panel-heading" align="center">
		    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11"><b>Explicación</b>
		 			</div>
		 			<div class="panel-body"   style="background-color: #FFFDE7; opacity: 1; width: 100%"  align="center">		  
		 				<div class="table-striped">
							<table class="table"> 
								<tr class="info">
									<th>CODIGO</th>
									<th>NOMBRE</th>
									<th align="center">VALOR</th>
									<th>PROFESIONES Y OCUPACIONES</th>
								</tr>
								@if($tipo==1)
								<tr >
									<td class="success">CCFM</td>
									<td>CIENCIAS FISICAS MATEMATICAS</td>
									<td align="center" class="warning">{{$a}}</td>
									<td> 
		 								Ingenierías: Civil, de Sistemas, Industrial, Electrónica, Textil, Química, etc. Arquitectura.  Carreras técnicas: Técnico en TV y radio, electricista, mecánico
		 							</td>
								</tr>
								@endif
								@if($tipo==2)
								<tr>
									<td class="success">CCSS</td>
									<td>CIENCIAS SOCIALES</td>
									<td align="center" class="warning">{{$a}}</td>
									<td>Educación inicial, primaria, secundaria, física y educación a nivel especial. Antropología, sociología, servicio social, Historia, arqueología, Fisiología, Teología, Antropología, PSICOLOGÍA</td>
								</tr>
								@endif
								@if($tipo==3)
								<tr>
									<td class="success">CCNA</td>
									<td>CIENCIAS NATURALES</td>
									<td align="center" class="warning">{{$ccna}}</td>
									<td>Medicina Humana, Obstetricia, Enfermería, Nutrición, Biología, Odontología, Químico-Farmacéutico, Medicina Veterinaria, Agronomía, Zootécnia. Carreras Técnicas: Agrotécnia, Auxiliar de enfermería</td>
								</tr>
								@endif
								@if($tipo==4)
								<tr>
									<td class="success">CCCO</td>
									<td>CIENCIAS DE LA COMUNICACIÓN</td>
									<td align="center" class="warning">{{$ccco}}</td>
									<td>Periodismo o Publicidad, Comunicación Audiovisual (Cine, radio y televisión) Relaciones Públicas Industriales, Turismo, Bibliotecología y Ciencias de la Información. </td>
								</tr>
								@endif
								@if($tipo==5)
								<tr>
									<td class="success">ARTE</td>
									<td>ARTES</td>
									<td align="center" class="warning">{{$arte}}</td>
									<td>Pintor, Actor, Escultor, Decorador de interiores, Diseñador de Modas, Director de cine y televisión, Músico, Profesor de Música, Crítico musical. Carreras técnicas: Artesanías en cerámica, técnico en Dibujo, Publicitario</td>
								</tr>
								@endif
								@if($tipo==6)
								<tr>
									<td class="success">BURO</td>
									<td>BUROCRACIA</td>
									<td align="center" class="warning">{{$buro}}</td>
									<td>Empleado de Oficina, Bibliotecario, Secretario, Archivista</td>
								</tr>
								@endif
								@if($tipo==10)
								<tr>
									<td class="success">CCEP</td>
									<td>CIENCIAS ECONOMICAS POLITICAS </td>
									<td align="center" class="warning">{{$ccep}}</td>
									<td>Economista, Estadista o Político, Diplomático</td>
								</tr>
								@endif
								@if($tipo==7)
								<tr>
									<td class="success">HAA</td>
									<td>INSTITUTOS ARMADOS </td>
									<td align="center" class="warning">{{$haa}}</td>
									<td>Oficial del Ejército, de la FAP, de la Marina, de la Policía</td>
								</tr>
								@endif
								@if($tipo==8)
								<tr>
									<td class="success">FINA</td>
									<td>FINANZAS </td>
									<td align="center" class="warning">{{$fina}}</td>
									<td>Contador, Bancario, Administrador de Empresas. Carreras técnicas: Auxiliar de Contabilidad, Secretario contable, Visitador médico</td>
								</tr>
								@endif
								@if($tipo==9)
								<tr>
									<td class="success">LING</td>
									<td>LINGUISTICA</td>
									<td align="center" class="warning">{{$ling}}</td>
									<td>Escritor, Lingüista, Traductor e interprete de idiomas. Carreras técnicas: Secretariado bilingüe</td>
								</tr>
								@endif
								@if($tipo==11)								
								<tr>
									<td class="success">JURI</td>
									<td>JURISPRUDECIA</td>
									<td align="center" class="warning">{{$juri}}</td>
									<td>Derecho (Penal, Civil, laboral), Notario Público. Carreras técnicas: Secretario legal</td>
								</tr>
								@endif
							</table>					
						</div>
		    		</div>	    		
		    	</div>
	    	</div>
    	</div>
    </div>
@endsection