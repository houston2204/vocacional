@extends('nuevo.index')
@section('titulo')
Test Vocacional
@endsection
@section('estilos')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans|Roboto" rel="stylesheet">
    <script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
@endsection
@section('contenido')	
	<div class="container" >
        <div class="row centered-form">
        	<div class="row centered-form">        	        	        	        
			<img  src="{{URL::to('img/fondo.jpg')}}" width="100%" style="position: absolute;top: 0px;left: 0px;z-index: 1; background-color:  rgba(255,255,255,0.5);opacity: 1;
			">
	        <div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1" style="z-index: 22; ">	     <br> <br> <br><br> 	
	        	<div class="panel panel-default" >
        			<div class="panel-heading" align="center">
		    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">{{$num}}/141 </span>
		 			</div>
		 			<div class="panel-body"   style="background-color: #FFFDE7; opacity: 1" >		 					 		    
			    		<form role="form" style="padding: 10px;" action="{{URL::to('test')}}" >	
			    			<div class="row">
			    				<div class="col-xs-12 col-sm-6 col-md-6">
			    					<div class="form-group formulario">			    																
			                			<div class="checkbox" id="opcion1">
											<input type="checkbox" name="a" id="checkbox1">
											<label  id="opcion1" for="checkbox1">Prefiere diseñar el modelo de casas, edificios, parques, etc.</label>	
										</div>
			    					</div>
			    				</div>				    				
			    				<div class="col-xs-12 col-sm-6 col-md-6">
			    					<div class="form-group formulario">			    																
			                			<div class="checkbox">
											<input type="checkbox" name="b" id="checkbox2">
											<label id="opcion2" for="checkbox2">Le gusta resolver problemas de matemáticas</label>						
										</div>
			    					</div>
			    				</div>			    				
			    				<input type="hidden" name="numero" value="1">  			    				
				    		</div>	    					    							    		
			    			<input type="submit" value="Siguiente" class="btn btn-succes btn-block" style="background-color: #00c853; font-family: 'Roboto', sans-serif;  font-size: 1.5em; letter-spacing: 2px; color: white"  >	
			    			<input type="hidden" name="numero" value=" {{$num}}">  			    	
			    			<input type="hidden" name="usuario" value="{{$usuario}}">		
			    		</form>
		    		</div>
		    	</div>
	    	</div>	    	
    	</div>
    	</div>
    </div>
@endsection
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script>   	
            $("#opcion1").click(function() {
  				$('#checkbox1').text('a');  				
			});	  
			$("#opcion2").click(function() {
  				$('#checkbox1').text('b');  				
			});	
	</script>
@endsection