@extends('nuevo.index')
@section('titulo')
Test Vocacional
@endsection
@section('estilos')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans|Roboto" rel="stylesheet">
    <script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,900" rel="stylesheet"> 
@endsection
@section('contenido')	
	<div class="container" >
        <div class="row centered-form">        		       			
	        <div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/1')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Ciencias fisico matemáticas</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/ccfm.png')}}" width="100%" alt=""> 
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div>
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/2')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Ciencias sociales</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/ccss.png')}}" width="100%"  alt="">
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div>
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/3')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Ciencias naturales</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/ccna.png')}}" width="100%"  alt="">
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div> 
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/4')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Ciencias de la comunicación</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/ccco.png')}}" width="100%"  alt=""> 
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div> 
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/5')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">ARTES</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/arte.png')}}" width="100%"  alt="">
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div> 
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/6')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Burocracia</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/buro.png')}}" width="100%"  alt=""> 
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div> 
	    	
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/7')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Institutos armados</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/haa.png')}}" width="100%"  alt="">
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div>
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/8')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Finanzas</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/fina.png')}}" width="100%"  alt="">
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div>  	
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/9')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Lingüística</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/ling.png')}}" width="100%"  alt="">
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div>  	
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/10')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Ciencias económicas y políticas</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/ccep.png')}}" width="100%"  alt=""> 
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div>
	    	<div class="col-xs-12 col-sm-12 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('inicio/11')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">Jurisprudencia</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<img src="{{URL::to('img/juri.png')}}" width="100%"  alt="">
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 4 minutos</p>			    			
			    			<p><b>CANTIDAD:</b> 22 preguntas</p>
			    		</div>
			    	</div>
			    </a>
	    	</div>   	  	
    	</div>
    </div>
@endsection
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script>   	
            $("#opcion1").click(function() {
  				$('#checkbox1').text('a');  				
			});	  
			$("#opcion2").click(function() {
  				$('#checkbox1').text('b');  				
			});	
	</script>
@endsection