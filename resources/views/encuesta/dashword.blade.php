@extends('nuevo.index')
@section('titulo')
Test Vocacional
@endsection
@section('estilos')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans|Roboto" rel="stylesheet">
    <script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,900" rel="stylesheet"> 
@endsection
@section('contenido')	
	<div class="container" >
        <div class="row centered-form">
        {{-- <img  src="{{URL::to('img/fondo.jpg')}}" width="100%" style="position: absolute;top: 0px;left: 0px;z-index: 1; background-color:  rgba(255,255,255,0.5);opacity: 1;
			">        		       			 --}}
	        <div class="col-xs-6 col-sm-6 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URL::to('complete/0')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">COMPLETO</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<p>
			 					En este cuestionario podrás observar las  143 preguntas que mediran tus intereces en 11 areas especificas.
			 				</p> 
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 20 minutos</p>		    			
			    		</div>
			    	</div>
			    </a>
	    	</div>
	    	<div class="col-xs-6 col-sm-6 col-md-6 " style="z-index: 10000;">	 <br><br>       	
		        <a href="{{URl::to('parts')}}" style="text-decoration:none; color: black; font-size: 16px;">
		        	<div class="panel panel-default" >
	        			<div class="panel-heading" align="center">
			    			<span style=" font-family: 'Roboto', sans-serif; font-size: 2em; z-index: 11">SIMPLIFICADO</span>
			 			</div>
			 			<div class="panel-body"   style="background-color: #fafafa; opacity: 1">		 				
			 				<p>
			 					Esta seccción tenemos 11 areas especificas que me dirán tu interes  en un area específico.
			 				</p> 
			    		</div>
			    		<div class="panel-footer">
			    			<p><b>TIPO:</b> Cuestionario</p>		    			
			    			<p><b>DURACIÓN:</b> 20 minutos</p>		    			
			    		</div>
			    	</div>
			    </a>
	    	</div>    	
    	</div>
    </div>
@endsection
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script>   	
            $("#opcion1").click(function() {
  				$('#checkbox1').text('a');  				
			});	  
			$("#opcion2").click(function() {
  				$('#checkbox1').text('b');  				
			});	
	</script>
@endsection