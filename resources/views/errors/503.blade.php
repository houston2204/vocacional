<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://fonts.google.com/specimen/Baloo+Tamma">
        <style>
            @import 'https://fonts.googleapis.com/css?family=Lato|Montserrat|Open+Sans|Raleway|Roboto';
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                /*color: #B0BEC5;*/
                color: white;
                display: table;
                font-weight: 100;
                /*font-family: 'Lato';*/
                font-family: 'Open Sans', sans-serif;
                background: #3385ff;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
            .link{
                font-size: 35px;
                margin-bottom: 40px;                   
            }
            .link >a{
                text-decoration: none;
                color: #ffd11a;
                cursor: pointer;
            }
        </style>        
    </head>
    <body>
        <div class="container">
            <div class="content">
                <img src="<?=URL::to('img/error404.png') ?>" width="340">
                <div class="title">Hmmm!<br> No conseguimos esta pagina</div>
                <div class="link"><a href="<?=URL::to('/') ?>">Volver</a></div>
            </div>
        </div>
    </body>
</html>
