<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ling extends Model
{
    protected $table ='ling';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
