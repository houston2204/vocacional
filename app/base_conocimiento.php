<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class base_conocimiento extends Model
{
    protected $table ='base_conocimiento';
    public $timestamps= false;    
    protected $fillable =['id','a','b'];
}
