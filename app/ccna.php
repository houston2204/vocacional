<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ccna extends Model
{
    protected $table ='ccna';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
