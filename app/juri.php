<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class juri extends Model
{
    protected $table ='juri';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
