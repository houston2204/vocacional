<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ccss extends Model
{
    protected $table ='ccss';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
