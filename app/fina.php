<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fina extends Model
{
    protected $table ='fina';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
