<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pregutas extends Model
{
    protected $table ='test';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
