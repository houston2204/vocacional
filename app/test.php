<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class test extends Model
{
    protected $table ='test';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
