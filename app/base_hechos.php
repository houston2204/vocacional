<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class base_hechos extends Model
{
    protected $table ='base_hechos';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
