<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ccfm extends Model
{
    protected $table ='ccfm';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
