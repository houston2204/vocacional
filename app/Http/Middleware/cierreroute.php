<?php

namespace App\Http\Middleware;

use Closure;
use App\cierre;
use Auth;
use Session;
use App\tienda;
class cierreroute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {       
        if(Auth::user()->nivel<'4'){
            if(count(tienda::find(Auth::user()->tienda))){
        if(!count(cierre::where('usuario',Auth::user()->id)->where('tienda',Auth::user()->tienda)->where('estado','1')->first())){
            return redirect()->route('cierre.index');}
        else
        return $next($request);}
        else 
        {

            Session::flash('message','Usuario no asignado a tienda');
            return redirect()->back(); 
        }
        }


        else {

            Session::flash('message','Solo dueños y trabajadores en esta seccion');
            return redirect()->back();
        }
        

    
    }
}

