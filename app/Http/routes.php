<?php
Route::get('error', function(){
	return   view('errors.503');
});

Route::get('/', function(){
	return view('encuesta.dashword');
});
Route::get('/index2',function(){
	return view('encuesta.dashword');
});
Route::get('parts',function(){
	return view('encuesta.index_part');
});
Route::get('complete/{id}',function($id){
	return view('encuesta.index',compact('id'));
});

Route::get('inicio/{id}',function($id){
	return view('encuesta.index',compact('id'));
});

Route::get('registrar/{id}', 'UsuarioController@tipo');

Route::get('encuesta', function(){
	return   view('encuesta.encuesta');
});

// Route::get('test', 'ControllerBase_Hecho@store');
Route::post('test', 'ControllerBase_Hecho@store');


Route::get('hechos/{a}/{b}/{tipo}/{contador}/{pregunta}/{usuario}', 'ControllerBase_Hecho@save');

Route::get('veracidad/{id}','ControllerMotor_inferencia@veracidad');

Route::get('validez/{id}','ControllerMotor_inferencia@validez');

Route::get('ccfm/{id}','ControllerMotor_inferencia@ccfm');

Route::get('ccss/{id}','ControllerMotor_inferencia@ccss');

Route::get('ccna/{id}','ControllerMotor_inferencia@ccna');

Route::get('ccco/{id}','ControllerMotor_inferencia@ccco');

Route::get('arte/{id}','ControllerMotor_inferencia@arte');

Route::get('buro/{id}','ControllerMotor_inferencia@buro');

Route::get('ccep/{id}','ControllerMotor_inferencia@ccep');

Route::get('haa/{id}','ControllerMotor_inferencia@haa');

Route::get('fina/{id}','ControllerMotor_inferencia@fina');

Route::get('ling/{id}','ControllerMotor_inferencia@ling');

Route::get('juri/{id}','ControllerMotor_inferencia@juri');

Route::get('resultado', function(){
	return   view('encuesta.resultado');
});
