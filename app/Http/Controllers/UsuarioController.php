<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\usuario;
use Illuminate\Support\Facades\DB;
use App\base_conocimiento;
use App\arte;
use App\buro;
use App\ccco;
use App\ccep;
use App\ccfm;
use App\ccna;
use App\ccss;
use App\fina;
use App\haa;
use App\juri;
use App\ling;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data=[
            'nombre' =>'usuario',
            ];    
        
        $tipo = $request->get('tipo');
        return $tipo;
        $usuario = usuario::create($data);        
        
        $usuario = usuario::find($usuario->id)->id;

        // return view('encuesta.pregunta1',compact(varname));
        $alternativa_a = base_conocimiento::find(1)->a;
        $alternativa_b = base_conocimiento::find(1)->b;
        $pregunta = '1'; 
        return view('encuesta.encuesta',compact('usuario','alternativa_a','alternativa_b','pregunta'));
    }

    public function tipo($id)
    {
        $data=[ 'nombre' =>$id ];   
        $usuario  = usuario::create($data);                
        $usuario = $usuario->id; 
        $numero = 1;
        $pregunta = 1;        
        $contador = 22;

        if($id==0) {                        

            $alternativa_a = base_conocimiento::find($numero)->a;
            $alternativa_b = base_conocimiento::find($numero)->b;            
            $contador=143;

        }elseif ($id == 1) {

            $alternativa_a = ccfm::find($numero)->a;
            $alternativa_b = ccfm::find($numero)->b;

        }elseif ($id == 2) {

            $alternativa_a = ccss::find($numero)->a;
            $alternativa_b = ccss::find($numero)->b;

        }elseif ($id == 3) {

            $alternativa_a = ccna::find($numero)->a;
            $alternativa_b = ccna::find($numero)->b;

        }elseif ($id == 4) {

            $alternativa_a = ccco::find($numero)->a;
            $alternativa_b = ccco::find($numero)->b;

        }elseif ($id ==5) {

            $alternativa_a = arte::find($numero)->a;
            $alternativa_b = arte::find($numero)->b;

        }elseif ($id ==6) {
            
            $alternativa_a = buro::find($numero)->a;
            $alternativa_b = buro::find($numero)->b;

        }elseif ($id ==7) {

            $alternativa_a = haa::find($numero)->a;
            $alternativa_b = haa::find($numero)->b;

        }elseif ($id==8) {

            $alternativa_a = fina::find($numero)->a;
            $alternativa_b = fina::find($numero)->b;

        }elseif ($id==9) {
            
            $alternativa_a = ling::find($numero)->a;
            $alternativa_b = ling::find($numero)->b;

        }elseif ($id==10) {
            
            $alternativa_a = ccep::find($numero)->a;
            $alternativa_b = ccep::find($numero)->b;

        }elseif ($id==11) {
            $alternativa_a = juri::find($numero)->a;
            $alternativa_b = juri::find($numero)->b;
        }
                
        $tipo = $id;

        return view('encuesta.encuesta',compact('usuario','alternativa_a','alternativa_b','pregunta','tipo','contador'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
