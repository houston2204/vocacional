<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\base_hechos;
use Illuminate\Support\Facades\DB;
use ArrayObject;


class ControllerMotor_inferencia extends Controller
{
   
    public function veracidad($id){
        $suma_a = 0;       

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();

        foreach ($hecho as $key=> $pregunta) {     
                        
            if($hecho[$key]->a==1 && $hecho[$key]->numero==12){
                $suma_a++;               
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==25) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==38) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==51) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==64) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==77) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==90) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==103) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==116) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==129) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==142) {
                $suma_a++;  
            }                  
        }
        // return $suma_a;
        if($suma_a >= 5){
            return "Prueba invalida";
        }else{
            return "Prueba valida";
        }
    }
    public function validez($id){
        $suma = 0;
        $total = 0; 
        //validacion item 13-131
        $v_1_a=0; $v_1_b=0; $aux_1_a; $aux_1_b;
        $v_2_a=0; $v_2_b=0; $aux_2_a; $aux_2_b;
        //validacion item 26-132
        $v_3_a=0; $v_3_b=0; $aux_3_a; $aux_3_b;
        $v_4_a=0; $v_4_b=0; $aux_4_a; $aux_4_b;
        //validación item 39-133
        $v_5_a=0; $v_5_b=0; $aux_5_a; $aux_5_b;
        $v_6_a=0; $v_6_b=0; $aux_6_a; $aux_6_b;
        //validación item 52-134
        $v_7_a=0; $v_7_b=0; $aux_7_a; $aux_7_b;
        $v_8_a=0; $v_8_b=0; $aux_8_a; $aux_8_b;
        //validación item 65-135
        $v_9_a=0; $v_9_b=0; $aux_9_a; $aux_9_b;
        $v_10_a=0; $v_10_b=0; $aux_10_a; $aux_10_b;
        //validación item 78-136
        $v_11_a=0; $v_11_b=0; $aux_11_a; $aux_11_b;
        $v_12_a=0; $v_12_b=0; $aux_12_a; $aux_12_b;
        //validación item 91-137
        $v_13_a=0; $v_13_b=0; $aux_13_a; $aux_13_b;
        $v_14_a=0; $v_14_b=0; $aux_14_a; $aux_14_b;
        //validación item 104-138
        $v_15_a=0; $v_15_b=0; $aux_15_a; $aux_15_b;
        $v_16_a=0; $v_16_b=0; $aux_16_a; $aux_16_b;
        //validación item 117-139
        $v_17_a=0; $v_17_b=0; $aux_17_a; $aux_17_b;
        $v_18_a=0; $v_18_b=0; $aux_18_a; $aux_18_b;
        //validación item 130-140
        $v_19_a=0; $v_19_b=0; $aux_19_a; $aux_19_b;
        $v_20_a=0; $v_20_b=0; $aux_20_a; $aux_20_b;
        //validación item 130-140
        $v_21_a=0; $v_21_b=0; $aux_21_a; $aux_21_b;
        $v_22_a=0; $v_22_b=0; $aux_22_a; $aux_22_b;



        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        foreach ($hecho as $key=> $pregunta) {     
            //13 a - b             
            if($hecho[$key]->numero == 13){
               
                $aux_1_a = $hecho[$key]->a;
                $v_1_a = $aux_1_a;

                $aux_1_b = $hecho[$key]->b;
                $v_1_b = $aux_1_b;                
            }elseif($hecho[$key]->numero == 131){
               
                $aux_2_a = $hecho[$key]->a;
                $v_2_a = $aux_2_a;

                $aux_2_b = $hecho[$key]->b;
                $v_2_b = $aux_2_b;                
            }elseif($hecho[$key]->numero == 26){
               
                $aux_3_a = $hecho[$key]->a;
                $v_3_a = $aux_3_a;

                $aux_3_b = $hecho[$key]->b;
                $v_3_b = $aux_3_b;                
            }elseif($hecho[$key]->numero == 132){
               
                $aux_4_a = $hecho[$key]->a;
                $v_4_a = $aux_4_a;

                $aux_4_b = $hecho[$key]->b;
                $v_4_b = $aux_4_b;                
            }elseif($hecho[$key]->numero == 39){
               
                $aux_5_a = $hecho[$key]->a;
                $v_5_a = $aux_5_a;

                $aux_5_b = $hecho[$key]->b;
                $v_5_b = $aux_5_b;                
            }elseif($hecho[$key]->numero == 133){
               
                $aux_6_a = $hecho[$key]->a;
                $v_6_a = $aux_6_a;

                $aux_6_b = $hecho[$key]->b;
                $v_6_b = $aux_6_b;                
            }elseif($hecho[$key]->numero == 52){
               
                $aux_7_a = $hecho[$key]->a;
                $v_7_a = $aux_7_a;

                $aux_7_b = $hecho[$key]->b;
                $v_7_b = $aux_7_b;                
            }elseif($hecho[$key]->numero == 134){
               
                $aux_8_a = $hecho[$key]->a;
                $v_8_a = $aux_8_a;

                $aux_8_b = $hecho[$key]->b;
                $v_8_b = $aux_8_b;                
            }elseif($hecho[$key]->numero == 65){
               
                $aux_9_a = $hecho[$key]->a;
                $v_9_a = $aux_9_a;

                $aux_9_b = $hecho[$key]->b;
                $v_9_b = $aux_9_b;                
            }elseif($hecho[$key]->numero == 135){
               
                $aux_10_a = $hecho[$key]->a;
                $v_10_a = $aux_10_a;

                $aux_10_b = $hecho[$key]->b;
                $v_10_b = $aux_10_b;                
            }elseif($hecho[$key]->numero == 78){
               
                $aux_11_a = $hecho[$key]->a;
                $v_11_a = $aux_11_a;

                $aux_11_b = $hecho[$key]->b;
                $v_11_b = $aux_11_b;                
            }elseif($hecho[$key]->numero == 136){
               
                $aux_12_a = $hecho[$key]->a;
                $v_12_a = $aux_12_a;

                $aux_12_b = $hecho[$key]->b;
                $v_12_b = $aux_12_b;                
            }elseif($hecho[$key]->numero == 91){
               
                $aux_13_a = $hecho[$key]->a;
                $v_13_a = $aux_13_a;

                $aux_13_b = $hecho[$key]->b;
                $v_13_b = $aux_13_b;                
            }elseif($hecho[$key]->numero == 137){
               
                $aux_14_a = $hecho[$key]->a;
                $v_14_a = $aux_14_a;

                $aux_14_b = $hecho[$key]->b;
                $v_14_b = $aux_14_b;                
            }elseif($hecho[$key]->numero == 104){
               
                $aux_15_a = $hecho[$key]->a;
                $v_15_a = $aux_15_a;

                $aux_15_b = $hecho[$key]->b;
                $v_15_b = $aux_15_b;                
            }elseif($hecho[$key]->numero == 138){
               
                $aux_16_a = $hecho[$key]->a;
                $v_16_a = $aux_16_a;

                $aux_16_b = $hecho[$key]->b;
                $v_16_b = $aux_16_b;                
            }elseif($hecho[$key]->numero == 117){
               
                $aux_17_a = $hecho[$key]->a;
                $v_17_a = $aux_17_a;

                $aux_17_b = $hecho[$key]->b;
                $v_17_b = $aux_17_b;                
            }elseif($hecho[$key]->numero == 139){
               
                $aux_18_a = $hecho[$key]->a;
                $v_18_a = $aux_18_a;

                $aux_18_b = $hecho[$key]->b;
                $v_18_b = $aux_18_b;                
            }elseif($hecho[$key]->numero == 130){
               
                $aux_19_a = $hecho[$key]->a;
                $v_19_a = $aux_19_a;

                $aux_19_b = $hecho[$key]->b;
                $v_19_b = $aux_19_b;                
            }elseif($hecho[$key]->numero == 140){
               
                $aux_20_a = $hecho[$key]->a;
                $v_20_a = $aux_20_a;

                $aux_20_b = $hecho[$key]->b;
                $v_20_b = $aux_20_b;                
            }elseif($hecho[$key]->numero == 143){
               
                $aux_21_a = $hecho[$key]->a;
                $v_21_a = $aux_21_a;

                $aux_21_b = $hecho[$key]->b;
                $v_21_b = $aux_21_b;                
            }elseif($hecho[$key]->numero == 1){
               
                $aux_22_a = $hecho[$key]->a;
                $v_22_a = $aux_22_a;

                $aux_22_b = $hecho[$key]->b;
                $v_22_b = $aux_22_b;                
            }            
            
        }

        if($v_1_a == $v_2_a && $v_1_b == $v_2_b)
        {
            $suma++;
        }
        if($v_3_a == $v_4_a && $v_3_b == $v_4_b) {
            $suma++;
        }
        if($v_5_a == $v_6_a && $v_5_b == $v_6_b) {
            $suma++;
        }
        if($v_7_a == $v_8_a && $v_7_b == $v_8_b) {
            $suma++;
        }
        if($v_9_a == $v_10_a && $v_9_b == $v_10_b) {
            $suma++;
        }
        if($v_11_a == $v_12_a && $v_11_b == $v_12_b) {
            $suma++;
        }
        if($v_13_a == $v_14_a && $v_13_b == $v_14_b) {
            $suma++;
        }
        if($v_15_a == $v_16_a && $v_15_b == $v_16_b) {
            $suma++;
        }
        if($v_17_a == $v_18_a && $v_17_b == $v_18_b) {
            $suma++;
        }
        if($v_19_a == $v_20_a && $v_19_b == $v_20_b) {
            $suma++;
        }
        if($v_21_a == $v_22_a && $v_21_b == $v_22_b) {
            $suma++;
        }
        // return $suma;
        $total = 11-$suma;
        if ($total > 5 ) {
            return 'Prueba invalida debe volver a efectuarla';        
        }else {
            return 'Prueba consistente';
        }       
    }
    public function ccfm($id){
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==1){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==1) {
                $suma_a++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==2) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==3) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==4) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==5) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==6) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==7) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==8) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==9) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==10) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==11) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==12) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==13) {
                $suma_b++;                      
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==14) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==27) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==40) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==53) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==66) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==79) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==92) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==105) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==118) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==131) {
                $suma_a++;  
            }              

        }
        $total =  $suma_a + $suma_b;
        return $total;
    }
    public function ccss($id){
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==2){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==14) {
                $suma_a++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==15) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==16) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==17) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==18) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==19) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==20) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==21) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==22) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==23) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==24) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==25) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==26) {
                $suma_b++;                      
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==15) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==28) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==41) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==54) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==67) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==80) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==93) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==106) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==119) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==132) {
                $suma_a++;  
            }              

        }
        $total =  $suma_a + $suma_b;
        return $total;
    }
    public function ccna($id){

        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==3){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==27) {
                $suma_a++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==28) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==29) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==30) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==31) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==32) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==33) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==34) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==35) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==36) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==37) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==38) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==39) {
                $suma_b++;                      
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==16) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==29) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==42) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==55) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==68) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==81) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==94) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==107) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==120) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==133) {
                $suma_a++;  
            }              
        }

        $total =  $suma_a + $suma_b;

        return $total;
    }
    public function ccco($id){
        
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==4){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==40) {
                $suma_a++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==41) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==42) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==43) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==44) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==45) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==46) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==47) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==48) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==49) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==50) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==51) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==52) {
                $suma_b++;                      
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==17) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==30) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==43) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==56) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==69) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==82) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==95) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==108) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==121) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==134) {
                $suma_a++;  
            }              
        }

        $total =  $suma_a + $suma_b;

        return $total;
    }
    public function arte($id){
        
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==5){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==53) {
                $suma_a++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==54) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==55) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==56) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==57) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==58) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==59) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==60) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==61) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==62) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==63) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==64) {
                $suma_b++;  
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==65) {
                $suma_b++;                      
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==18) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==31) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==44) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==57) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==70) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==83) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==96) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==109) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==122) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==135) {
                $suma_a++;  
            }              
        }

        $total =  $suma_a + $suma_b;

        return $total;
    }
    public function buro($id){
        
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==6){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==66) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==67) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==68) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==69) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==70) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==71) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==72) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==73) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==74) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==75) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==76) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==77) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==78) {
                $suma_b++;                     
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==19) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==32) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==45) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==58) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==71) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==84) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==97) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==110) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==123) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==136) {
                $suma_a++;  
            }              
        }

        $total =  $suma_a + $suma_b;
        return $total;
    }
    public function ccep($id){
        
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==7){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==79) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==80) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==81) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==82) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==83) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==84) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==85) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==86) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==87) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==88) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==89) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==90) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==91) {
                $suma_b++;                     
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==20) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==33) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==46) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==59) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==72) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==85) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==98) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==111) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==124) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==137) {
                $suma_a++;  
            }              
        }

        $total =  $suma_a + $suma_b;
        return $total;
    }
    public function haa($id){
        
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==8){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==92) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==93) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==94) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==95) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==96) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==97) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==98) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==99) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==100) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==101) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==102) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==103) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==104) {
                $suma_b++;                     
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==21) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==34) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==47) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==60) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==73) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==86) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==99) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==112) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==125) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==138) {
                $suma_a++;  
            }              
        }

        $total =  $suma_a + $suma_b;
        return $total;
    }
    public function fina($id){
        
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==9){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==105) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==106) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==107) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==108) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==109) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==110) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==111) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==112) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==113) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==114) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==115) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==116) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==117) {
                $suma_b++;                     
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==22) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==35) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==48) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==61) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==74) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==87) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==100) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==113) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==126) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==139) {
                $suma_a++;  
            }
        }

        $total =  $suma_a + $suma_b;
        return $total;
    }
    public function ling($id){
        
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==10){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==118) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==119) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==120) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==121) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==122) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==123) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==124) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==125) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==126) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==127) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==128) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==129) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==130) {
                $suma_b++;                     
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==23) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==36) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==49) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==62) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==75) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==88) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==101) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==114) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==127) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==140) {
                $suma_a++;  
            }
        }

        $total =  $suma_a + $suma_b;
        return $total;
    }
    public function juri($id){
        
        $suma_a = 0;       
        $suma_b = 0;
        $total = 0;

        $hecho = DB::table('base_hechos')->where('usuario',$id)->get();
        
        foreach ($hecho as $key=> $pregunta) {                         
            if($hecho[$key]->a==1 && $hecho[$key]->numero==11){
                $suma_a++;               
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==131) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==132) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==133) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==134) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==135) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==136) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==137) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==138) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==139) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==140) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==141) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==142) {
                $suma_b++; 
            }elseif ($hecho[$key]->b==1 && $hecho[$key]->numero==143) {
                $suma_b++;                     
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==24) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==37) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==50) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==63) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==76) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==89) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==102) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==115) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==128) {
                $suma_a++;  
            }elseif ($hecho[$key]->a==1 && $hecho[$key]->numero==141) {
                $suma_a++;  
            }
        }

        $total =  $suma_a + $suma_b;
        return $total;
    }
}



