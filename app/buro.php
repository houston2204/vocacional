<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buro extends Model
{
    protected $table ='buro';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
