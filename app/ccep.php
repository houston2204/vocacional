<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ccep extends Model
{
    protected $table ='ccep';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
