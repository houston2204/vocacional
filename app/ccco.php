<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ccco extends Model
{
    protected $table ='ccco';
    public $timestamps= false;    
    protected $fillable =['id','a','b','numero','usuario'];
}
