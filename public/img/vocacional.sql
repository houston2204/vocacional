-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-01-2017 a las 15:38:36
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vocacional`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arte`
--

CREATE TABLE `arte` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `arte`
--

INSERT INTO `arte` (`id`, `a`, `b`) VALUES
(1, 'Te gustaría estudiar escultura en la escuela de bellas artes', 'Preferiría ser parte de un elenco de teatro'),
(2, 'Le gusta trabajar haciendo instalaciones eléctricas', 'Prefiere dedicar su tiempo en la lectura de las novedades en la decoración de ambientes'),
(3, 'Le agrada mucho visitar el hogar de los trabajadores con el fin de verificar su verdadera situación social y económica', 'Prefiere trabajar en el decorado de tiendas y vitrinas'),
(4, 'Le gusta estudiar los recursos geográficos', 'Prefiere observar el comportamiento de las personas e imitarlas'),
(5, 'Le gustaría dedicar su tiempo a la organización de eventos deportivos entre dos o mas centros laborales', 'Preferiría dedicarse al estudio de la vida y obra de los grandes actores del cine y del teatro'),
(6, 'Le gusta trabajar de mecanógrafo (a)', 'Le gusta más dar forma a objetos moldeables, sea: plastilina, migas, arcilla, piedras, etc'),
(7, 'Le agrada mucho estudiar los fundamentos por los que una moneda se devalúa', 'Prefiere la lectura acerca de la vida y obra de grandes escultores como Miguel angel, Leonardo de Vinci, etc'),
(8, 'Le agrada mucho la vida del marinero', 'Prefiere combinar colores para expresar con naturalidad y belleza un paisaje'),
(9, 'Le gustaría trabajar tramitando la compra-venta de inmuebles', 'Prefiere utilizar las líneas y colores para expresar un sentimiento'),
(10, 'Le gusta estudiar las lenguas y dialectos aborígenes', 'Prefiere combinar sonidos para obtener una nueva melodía'),
(11, 'Le agrada tramitar judicialmente el reconocimiento de sus hijo', 'Le agrada más aprender a tocar algún instrumento musical'),
(12, 'Le gusta hacer tallado en madera', 'Prefiere calcular la cantidad de materiales para una construcción'),
(13, 'Le agrada diseñar: muebles, puertas, ventanas, etc', 'Prefiere dedicar su tiempo a conocer las costumbres y tradiciones de los pueblos'),
(14, 'Le agrada construir, muebles, puertas, ventanas, etc', 'Prefiere estudiar acerca de las enfermedades de las personas'),
(15, 'Le gusta proyectar el tipo de muebles, cortinas y adornos sea para una oficina o para un hogar', 'Prefiere trabajar como redactor en un diario o revista'),
(16, 'Le interesa mucho leer sobre la vida y obra de músicos famosos', 'Prefiere el tipo de trabajo de un empleado bancario'),
(17, 'Le gusta y practica el baile como expresión artística', 'Prefiere estudiar las bases de la organización política del Tahuantinsuyo'),
(18, 'Le gusta ser parte de una agrupación de baile y danzas', 'Preferiría pertenecer a la Fuerza Aérea'),
(19, 'Le gusta actuar, representando a distintos personajes', 'Le agrada más tener su propio negocio'),
(20, 'Le gusta diseñar y/o confeccionar: adornos, utensilios, etc., en cerámica, vidrio; etc\r\n', 'Prefiere traducir textos escritos en otros idiomas'),
(21, 'Le interesa diseñar y/o confeccionar artículos de cuero', 'Prefiere asumir la defensa legal en la demarcación de fronteras territoriales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `base_conocimiento`
--

CREATE TABLE `base_conocimiento` (
  `id` int(11) NOT NULL,
  `a` varchar(300) NOT NULL,
  `b` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `base_conocimiento`
--

INSERT INTO `base_conocimiento` (`id`, `a`, `b`) VALUES
(1, 'Te gusta resolver problemas de matemáticas', 'Prefiere diseñar el modelo de casas, edificios, parques, etc'),
(2, 'Le agrada observar la conducta de las personas y opinar sobre su personalidad', 'Prefiere expresar un fenómeno concreto en una ecuación matemática'),
(3, 'Le gusta caminar por los cerros buscando piedras raras', 'Prefiere diseñar viviendas de una Urbanización'),
(4, 'Le gusta escribir artículos deportivos para un diario', 'Prefiere determinar la resistencia de los materiales para una construcción'),
(5, 'Le gusta hacer tallado en madera', 'Prefiere calcular la cantidad de materiales para una construcción'),
(6, 'Le gusta ordenar y archivar documentos', 'Prefiere proyectar el sistema eléctrico para una construcción'),
(7, 'Le agrada dedicar su tiempo en el estudio de teorías económicas', 'Prefiere dedicar su tiempo en la lectura de revistas sobre mecánica'),
(8, 'Le gusta mucho la vida militar', 'Prefiere diseñar: máquinas, motores, etc, de alto rendimiento'),
(9, 'Le gusta estudiar acerca de cómo formar una cooperativa', 'Prefiere estudiar el lenguaje de computación IBM'),
(10, ' Le agrada estudiar la gramática', 'Prefiere estudiar las matemáticas'),
(11, 'Le interesa mucho ser abogado', 'Preferiría dedicarse a escribir un tratado de física-matemática'),
(12, 'Le cuenta a su madre y a su padre todas sus cosas', 'Prefiere ocultar algunas cosas para Ud.'),
(13, 'Le agrada estudiar la estructura atómica de los cuerpos', 'Prefiere asumir la defensa legal de alguna persona acusada por algún delito'),
(14, 'Le interesa mucho estudiar como funciona un computador', 'Prefiere el estudio de las leyes y principios de la conducta psicológica'),
(15, 'Le agrada analizar la forma como se organiza un pueblo', 'Prefiere el estudio de las leyes y principios de la conducta psicológica'),
(16, 'Le gusta analizar las rocas, piedras, tierra para averiguar su composición mineral', 'Prefiere el estudio de las organizaciones sean: campesinas, educativas, laborales, económicas, políticas o religiosas'),
(17, 'Le gusta escribir artículos culturales para un diario', 'Prefiere pensar largamente acerca de la forma como el hombre podría mejorar su existencia'),
(18, 'Le agrada diseñar: muebles, puertas, ventanas, etc', 'Prefiere dedicar su tiempo a conocer las costumbres y tradiciones de los pueblos'),
(19, 'Le gusta mucho conocer el trámite documentario de un ministerio público', 'Prefieres el estudio de las religiones'),
(20, 'Le interesa mucho conocer los mecanismos de la economía nacional', 'Prefiere ser guía espiritual de las personas'),
(21, 'Le interesa mucho tener bajo su mando a un grupo de soldados', 'Prefiere enseñar lo que sabe a un grupo de compañeros'),
(22, 'Le gusta ser parte de la administración de una cooperativa', 'Prefiere el estudio de las formas más efectivas para la enseñanza de jóvenes y  niños'),
(23, 'Le interesa mucho estudiar la raíz gramatical de las palabras de su idioma', 'Prefiere dedicar su tiempo en la búsqueda de huacos y ruinas'),
(24, 'Le agrada mucho estudiar el código del derecho civil', 'Prefiere el estudio de las culturas peruanas y de otras naciones'),
(25, 'Le agrada que sus hermanos o familiares lo vigilen constantemente', 'Prefiere que confíen en su buen criterio'),
(26, 'Le gustaría escribir un tratado acerca de la historia del Perú', 'Prefiere asumir la defensa legal de un acusado por narcotráfico'),
(27, 'Le gusta proyectar las redes de agua y desagüe de una ciudad', 'Prefiere estudiar acerca de las enfermedades de la dentadura'),
(28, 'Le gusta visitar museos arqueológicos y conocer la vivienda y otros utensilios de nuestros antepasados', 'Prefiere hacer moldes para una dentadura postiza'),
(29, 'Le gusta recolectar plantas y clasificarlas por especies', 'Prefiere leer sobre el origen y funcionamiento de las plantas y animales'),
(30, 'Le gusta saber como se organiza una editorial periodística', 'Prefiere conocer las características de los órganos humanos y como funcionan'),
(31, 'Le agrada construir; muebles, puertas, ventanas, etc', 'Prefiere estudiar acerca de las enfermedades de las personas\r\n'),
(32, ' agradaría trabajar en la recepción y trámite documentario de una oficinapública', 'Prefiere experimentar con las plantas para obtener nuevas especies'),
(33, 'Le gusta proyectar los mecanismos de inversión económica de unaempresa', 'Prefiere analizar las tierras para obtener mayor producción agropecuaria'),
(34, 'Le agrada recibir y ejecutar órdenes de un superior', 'Prefiere el estudio de los órganos de los animales y su funcionamiento'),
(35, 'Le gusta saber mucho sobre los principios económicos de una cooperativa', 'Prefiere conocer las enfermedades que aquejan, sea: el ganado, aves, perros, etc'),
(36, 'Le agrada estudiar los fenómenos (sonidos verbales) de su idioma, o de otros', 'Prefiere dedicar mucho de su tiempo en el estudio de la química'),
(37, 'Le agrada defender pleitos judiciales de recuperación de tierras', 'Prefiere hacer mezclas de sustancias químicas para obtener derivados con fines productivos'),
(38, 'Sus amigos saben todo de usted, para ellos no tiene secretos', 'Prefiere reservar algo para usted solo (a) algunos secretos'),
(39, 'Le gusta investigar acerca de los recursos naturales de nuestro país (su fauna, su flora y suelo)', 'Prefiere estudiar derecho internacional'),
(40, 'Le gusta desarrollar programas de computación para proveer de información rápida y eficiente: a una empresa, institución, etc', 'Prefiere obtener fotografías que hagan noticia'),
(41, 'Le gusta mucho conocer el problema de las personas y tramitar su solución', 'Prefiere dedicar su tiempo a la búsqueda de personajes que hacen noticia'),
(42, 'Le gusta estudiar las características territoriales de los continentes', 'Prefiere entrevistar a políticos con el propósito de establecer su posición frente a un problema'),
(43, 'Le gusta conocer el funcionamiento de las máquinas impresoras de periódicos', 'Prefiere trabajar en el montaje fotográfico de un diario o revista'),
(44, 'Le gusta proyectar el tipo de muebles, cortinas y adornos sea para una oficina o para un hogar', 'Prefiere trabajar como redactor en un diario o revista'),
(45, 'Le gusta redactar cartas comerciales, al igual que oficios y solicitudes', 'Prefiere averiguar lo que opina el público respecto a un producto'),
(46, 'Le gusta estudiar las leyes de la oferta y la demanda', 'Prefiere redactar el tema para un anuncio publicitario'),
(47, 'Le gusta organizar el servicio de inteligencia de un cuartel', 'Prefiere trabajar en una agencia de publicidad'),
(48, 'Le gusta trabajar buscando casas de alquiler para ofrecerlas al público', 'Prefiere estudiar las características psicológicas para lograr un buen impacto publicitario'),
(49, ' Le interesa investigar acerca de cómo se originaron los idiomas', 'Prefiere preparar y ejecutar encuestas para conocer la opinión de las personas'),
(50, 'Le agrada hacer los trámites legales de un juicio de divorcio', 'Prefiere trabajar estableciendo contactos entre una empresa y otra'),
(51, 'Cuando está dando un examen y tiene la oportunidad de verificar una respuesta, nunca lo hace', 'Prefiere aprovechar la seguridad que la ocasión le confiere'),
(52, 'Le interesa investigar sobre los problemas del lenguaje en la comunicación masiva', 'Prefiere redactar documentos legales para contratos internacionales'),
(53, 'Le gusta trabajar haciendo instalaciones eléctricas', 'Prefiere dedicar su tiempo en la lectura de las novedades en la decoración de ambientes'),
(54, 'Le agrada mucho visitar el hogar de los trabajadores con el fin de verificar su verdadera situación social y económica', 'Prefiere trabajar en el decorado de tiendas y vitrinas'),
(55, 'Le gusta estudiar los recursos geográficos', 'Prefiere observar el comportamiento de las personas e imitarlas'),
(56, 'Le gustaría dedicar su tiempo a la organización de eventos deportivos entre dos o mas centros laborales', 'Preferiría dedicarse al estudio de la vida y obra de los grandes actores del cine y del teatro'),
(57, 'Le gustaría estudiar escultura en la escuela de bellas artes', 'Preferiría ser parte de un elenco de teatro'),
(58, 'Le gusta trabajar de mecanógrafo (a)', 'Le gusta más dar forma a objetos moldeables; sea: plastilina, migas, arcilla, piedras, etc\n'),
(59, 'Le agrada mucho estudiar los fundamentos por los que una moneda se devalúa', 'Prefiere la lectura acerca de la vida y obra de grandes escultores como Miguel angel, Leonardo de Vinci, etc'),
(60, 'Le agrada mucho la vida del marinero', 'Prefiere combinar colores para expresar con naturalidad y belleza un paisaje'),
(61, 'Le gustaría trabajar tramitando la compra-venta de inmuebles', 'Prefiere utilizar las líneas y colores para expresar un sentimiento'),
(62, 'Le gusta estudiar las lenguas y dialectos aborígenes', 'Prefiere combinar sonidos para obtener una nueva melodía'),
(63, 'Le agrada tramitar judicialmente el reconocimiento de sus hijo', 'Le agrada más aprender a tocar algún instrumento musical'),
(64, 'Si pasa por un cine y descubre que no hay vigilancia, no se aprovecha de la situación', 'Prefiere aprovechar la ocasión para entrar sin pagar su boleto'),
(65, 'Le interesa más diseñar y/o confeccionar artículos de cuero', ' Prefiere asumir la defensa legal en la demarcación de fronteras'),
(66, ' Prefiere estudiar acerca de cómo la energía se transforma en imágenes de radio, tv, etc', 'Le gusta tomar apuntes textuales o didácticos de otras personas'),
(67, 'Le gusta leer sobre la vida y obra de los santos religiosos', 'Prefiere hacer catálogos o listados de los libros de una biblioteca'),
(68, 'Le gusta dedicar mucho de su tiempo en la lectura de la astronomía', 'prefiere trabajar clasificando los libros por autores'),
(69, ' Le gusta trabajar defendiendo el prestigio de su centro laboral', 'Prefiere trabajar recibiendo y entregando documentos valorados como: cheques, giros, libretas de ahorros, etc'),
(70, 'Le interesa mucho leer sobre la vida y obra de músicos famosos', 'Prefiere el tipo de trabajo de un empleado bancario'),
(71, 'Le interesa mucho conseguir un trabajo en un banco comercial', 'Prefiere dedicarse a clasificar libros por especialidades'),
(72, 'Le gusta dedicar su tiempo en el conocimiento del por qué ocurre la inflación económica', 'Prefiere dedicarse al estudio de cómo se organiza una biblioteca'),
(73, 'Le interesa mucho el conocimiento de la organización de un buque de guerra', 'Prefiere dedicarse a la recepción y comunicación de mensajes sean verbales o por escrito'),
(74, 'Le gusta trabajar tramitando la compra-venta de vehículos motorizados', 'Prefiere transcribir los documentos de la administración pública'),
(75, 'Le gusta dedicar gran parte de su tiempo al estudio de las normas y reglas para el uso adecuado del lenguaje', 'Prefiere trabajar como secretario adjunto al jefe'),
(76, 'Le gusta dedicar su tiempo planteando la defensa de un juicio de alquiler', 'Prefiere asesorar y aconsejar en torno a tramites documentarios'),
(77, 'Si en la calle se encuentra dinero, sin documento alguno acude a la radio, TV para buscar al infortunado', 'Preferiría quedarse con el dinero, pues no se conoce al dueño'),
(78, 'Le interesa trabajar en la implementación de bibliotecas distritales', 'Prefiere asumir la responsabilidad legal para que un fugitivo, con residencia en otro país, sea devuelto a su país'),
(79, 'Le gusta estudiar acerca de cómo la energía se transforma en movimiento', 'Preferiría hacer una tesis sobre manejo económico para el país'),
(80, 'Le agrada leer sobre la vida y obra de grandes personajes de educación, sean: profesores, filósofos, psicólogos', 'Prefiere estudiar acerca de las bases económicas de un país'),
(81, 'Le gusta estudiar los astros; sus características, origen y evolución', 'Prefiere establecer comparaciones entre los sistemas y modelos económicos del mundo'),
(82, 'Le gustaría trabajar exclusivamente promocionando la imagen de su centro laboral', 'Prefiere estudiar las grandes corrientes ideológicas del mundo'),
(83, 'Le gusta y practica el baile como expresión artística', 'Prefiere estudiar las bases de la organización política del Tahuantinsuyo'),
(84, 'Le gusta mucho saber sobre el manejo de los archivos públicos', 'Prefiere establecer diferencias entre los distintos modelos políticos'),
(85, 'Le gusta investigar sobre las características de los regímenes totalitarios, democráticos, republicanos, etc', 'Prefiere ser el representante de su país en el extranjero'),
(86, 'Le gusta ser capitán de un buque de guerra', 'Le interesa más formar y conducir grupos con fines políticos'),
(87, 'Le agrada ser visitador médico', 'Prefiere dedicar su tiempo en la lectura de la vida y obra de los grandes politicos'),
(88, 'Siente placer buscando en el diccionario el significado de palabras nuevas', 'Prefiere dedicar todo su tiempo en aras de la paz entre las naciones'),
(89, 'Le interesa mucho estudiar el código penal', 'Prefiere estudiar los sistemas políticos de otros países'),
(90, 'Le agradan que le dejen muchas tareas para su casa', 'Prefiere que estas sean lo necesario para aprender'),
(91, 'Le agrada ser miembro activo de una agrupación política', 'Prefiere escuchar acusaciones y defensas para sancionar de acuerdo a lo que la ley señala'),
(92, 'Le gusta hacer los cálculos para el diseño de telas a gran escala', 'Le interesa más la mecánica de los barcos y submarinos'),
(93, 'Le agrada observar y evaluar como se desarrolla la inteligencia y personalidad', 'Prefiere ser aviador'),
(94, 'Le gustaría dedicar su tiempo en el descubrimiento de nuevos medicamentos', 'Prefiere dedicarse a la lectura acerca de la vida y obra de reconocidos militares, que han aportado en la organización de su institución'),
(95, 'Le gusta la aventura cuando está dirigida a descubrir algo que haga noticia', 'Prefiere conocer el mecanismo de los aviones de guerra'),
(96, 'Le gusta ser parte de una agrupación de baile y danzas', 'Preferiría pertenecer a la Fuerza Aérea'),
(97, 'Le gusta el trabajo de llevar mensajes de una dependencia a otra', 'Prefiere ser miembro de la Policía'),
(98, 'Le gustaría trabajar estableciendo vínculos culturales con otros países', 'Prefiere el trabajo en la detección y comprobación del delito'),
(99, 'Le gusta trabajar custodiando el orden público', 'Prefiere ser vigilante receloso de nuestras fronteras'),
(100, 'Le gusta persuadir a los boticarios en la compra de nuevos medicamentos', 'Prefiere trabajar vigilando a los presos en las prisiones'),
(101, 'Le apasiona leer de escritores serios y famosos', 'Prefiere organizar el servicio de inteligencia en la destrucción del narcotrafico'),
(102, 'Le gusta asumir la defensa legal de una persona acusada de robo', 'Prefiere conocer el mecanismo de las armas de fuego'),
(103, 'Se aleja Ud. cuando sus amistades cuentan chistes colorados', 'Prefiere quedarse gozando de la ocasión'),
(104, 'Le interesa mucho saber cómo se organiza un ejercito', 'Prefiere participar como jurado de un juicio'),
(105, 'Le gusta proyectar la extracción de metales de una mina', 'Prefiere estudiar el nombre de los medicamentos y su ventaja comercial'),
(106, 'Le gusta descifrar los diseños gráficos y escritos de culturas muy antiguas', 'Prefiere persuadir a la gente para que compre un producto'),
(107, 'Le agrada el estudio de los mecanismos de la visión y de sus enfermedades', 'Prefiere vender cosas'),
(108, 'Le gustaría ganarse la vida escribiendo para un diario o revista', 'Prefiere estudiar el mercado y descubrir el producto de mayor demanda'),
(109, 'Le gusta actuar, representando a distintos personajes', 'Le agrada más tener su propio negocio'),
(110, 'Le gusta sentirse importante sabiendo que de usted depende la rapidez o la lentitud de una solicitud', 'Prefiere trabajar en un bazar'),
(111, 'Le gusta planificar sea para una empresa local o a nivel nacional', 'Prefiere el negocio de una bodega o tienda de abarrotes'),
(112, 'Le interesa mucho utilizar sus conocimientos en la construcción de armamentos', 'Prefiere organizar empresas de finanzas y comercio'),
(113, 'Le agrada llevar la contabilidad de una empresa o negocio', 'Prefiere hacer las planillas de pago para los trabajadores de una empresa o institución'),
(114, 'Le agrada escribir cartas y luego hacer tantas correcciones como sean necesarias', 'Prefiere ser incorporado como miembros de la corporación nacional de comercio'),
(115, 'Le gusta asumir la defensa legal de una persona acusada de asesinato', 'Prefiere ser incorporado como miembro de la corporación nacional de  comercio'),
(116, 'Le agrada vestir todos los días muy formalmente (con terno y corbata por ejemplo)', 'Prefiere reservar esa vestimenta para ciertas ocasiones'),
(117, 'Le gusta evaluar la producción laboral de un grupo de trabajadores', 'Prefiere plantear, previa investigación, la acusación de un sujeto que ha actuado en contra de la ley'),
(118, 'Le gusta estudiar acerca de los reactores atómicos', 'Prefiere el estudio de las distintas formas literarias'),
(119, 'Le agrada estudiar en torno de la problemática social del Perú', 'Prefiere escribir cuidando mucho ser comprendido al tiempo que sus escritos resulten agradables al lector'),
(120, 'Le gustaría escribir un tratado sobre anatomía humana', 'Prefiere recitar sus propios poemas'),
(121, 'Le gustaría incorporarse al colegio de periodistas del Perú', 'Prefiere aprender otro idioma'),
(122, 'Le gusta diseñar y/o confeccionar: adornos, utensilios, etc., en cerámica, vidrio; etc', 'Prefiere traducir textos escritos en otros idiomas'),
(123, 'Le gustaría desarrollar técnicas de mayor eficiencia en el trámite documentario de un ministerio público', 'Prefiere escribir en otro idioma'),
(124, 'Le agradaría mucho ser secretario general de una central sindical', 'Prefiere dedicar su tiempo al estudio de lenguas extintas (muertas)'),
(125, 'Le gustaría dedicarse al estudio de normas de alta peligrosidad', 'Prefiere trabajar como traductor'),
(126, 'Le gusta llevar la estadística de ingresos y egresos mensuales de una empresa o tal vez de una nación', 'Prefiere los cursos de idiomas: Inglés, Francés, Italiano, etc'),
(127, 'Le gustaría ser incorporado como miembro de la Real Academia de la Lengua Española', 'Prefiere ser incorporado al Instituto Nacional del Idioma'),
(128, 'Le interesaría ser el asesor legal de un ministro de estado', 'Prefiere aquellas situaciones que le inspiran a escribir'),
(129, 'Nunca ha bebido licor, aún en ciertas ocasiones lo ha rechazado', 'Por lo contrario se ha adecuado a las circunstancias'),
(130, 'Le agrada dedicar muc ho de su tiempo en la escritura de poemas, cuentos, etc', 'Prefiere sentirse importante al saber que de su defensa legal depende la libertad de una  persona'),
(131, 'Le agrada estudiar la estructura atómica de los cuerpos', 'Prefiere asumir la defensa legal de una persona acusada por algún delito'),
(132, 'Le gustaría escribir un tratado acerca de la historia del Perú', 'Prefiere asumir la defensa legal de un acusado por narcotráfico'),
(133, 'Le gusta investigar de los recursos naturales de nuestro país (su fauna, su flora, su suelo)', 'Prefiere estudiar el derecho internacional'),
(134, 'Le interesa investigar sobre los problemas del lenguaje en la comunicación masiva', 'Prefiere redactar documentos legales para contratos internacionales'),
(135, 'Le interesa diseñar y/o confeccionar artículos de cuero', 'Prefiere asumir la defensa legal en la demarcación de fronteras territoriales'),
(136, 'Le interesa trabajar en la implementación de bibliotecas distritales', 'Prefiere asumir la responsabilidad legal para que un fugitivo con residencia en otro país sea devuelto a su país'),
(137, 'Le agrada ser miembro activo de una agrupación política', 'Prefiere escuchar acusaciones y defensas para sancionar de acuerdo a lo que la ley señala'),
(138, 'Le interesa mucho saber como se organiza un ejército', 'Prefiere participar como jurado en un juicio'),
(139, 'Le gusta evaluar la producción laboral de un grupo de trabajadores', 'Prefiere plantear previa investigación la acusación de un sujeto que ha ido en contra de la ley'),
(140, 'Le gusta dedicar mucho de su tiempo en la escritura de poemas, cuentos', 'Prefiere sentirse importante al saber que de su defensa legal depende la libertad de una persona'),
(141, 'Le gustaría dedicarse a la legalización de documentos (contratos, cartas, partidas, títulos, etc.)', 'Prefiere ser incorporado en una comisión para redactar un proyecto de ley'),
(142, ' Le agrada viajar en un microbús repleto de gente aún cuando no tiene ningún apuro', 'Prefiere esperar otro vehículo'),
(143, 'Le gusta resolver problemas matemáticos', 'Prefiere diseñar el modelo de casas, edificios, parques, etc');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `base_hechos`
--

CREATE TABLE `base_hechos` (
  `id` int(11) NOT NULL,
  `a` varchar(50) DEFAULT NULL,
  `b` varchar(50) DEFAULT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `usuario` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buro`
--

CREATE TABLE `buro` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `buro`
--

INSERT INTO `buro` (`id`, `a`, `b`) VALUES
(1, 'Le interesa mucho conseguir un trabajo en un banco comercial', 'Prefiere dedicarse a clasificar libros por especialidades'),
(2, ' Prefiere estudiar acerca de cómo la energía se transforma en imágenes de radio, tv, etc', 'Le gusta tomar apuntes textuales o didácticos de otras personas'),
(3, 'Le gusta leer sobre la vida y obra de los santos religiosos', 'Prefiere hacer catálogos o listados de los libros de una biblioteca'),
(4, 'Le gusta dedicar mucho de su tiempo en la lectura de la astronomía', 'prefiere trabajar clasificando los libros por autores'),
(5, ' Le gusta trabajar defendiendo el prestigio de su centro laboral', 'Prefiere trabajar recibiendo y entregando documentos valorados como: cheques, giros, libretas de ahorros, etc'),
(6, 'Le interesa mucho leer sobre la vida y obra de músicos famosos', 'Prefiere el tipo de trabajo de un empleado bancario'),
(7, 'Le gusta dedicar su tiempo en el conocimiento del por qué ocurre la inflación económica', 'Prefiere dedicarse al estudio de cómo se organiza una biblioteca'),
(8, 'Le interesa mucho el conocimiento de la organización de un buque de guerra', 'Prefiere dedicarse a la recepción y comunicación de mensajes sean verbales o por escrito'),
(9, 'Le gusta trabajar tramitando la compra-venta de vehículos motorizados', 'Prefiere transcribir los documentos de la administración pública'),
(10, 'Le gusta dedicar gran parte de su tiempo al estudio de las normas y reglas para el uso adecuado del lenguaje', 'Prefiere trabajar como secretario adjunto al jefe'),
(11, 'Le gusta dedicar su tiempo planteando la defensa de un juicio de alquiler', 'Prefiere asesorar y aconsejar en torno a tramites documentarios'),
(12, 'Le gusta ordenar y archivar documentos', 'Prefiere proyectar el sistema eléctrico para una construcción'),
(13, 'Le gusta mucho conocer el trámite documentario de un ministerio público', 'Prefieres el estudio de las religiones'),
(14, 'Le agradaría trabajar en la recepción y trámite documentario de una oficinapública', 'Prefiere experimentar con las plantas para obtener nuevas especies'),
(15, 'Le gusta redactar cartas comerciales, al igual que oficios y solicitudes', 'Prefiere averiguar lo que opina el público respecto a un producto'),
(16, 'Le gusta trabajar de mecanógrafo (a)', 'Le gusta más dar forma a objetos moldeables, sea: plastilina, migas, arcilla, piedras, etc'),
(17, 'Le gusta mucho saber sobre el manejo de los archivos públicos', 'Prefiere establecer diferencias entre los distintos modelos políticos'),
(18, 'Le gusta el trabajo de llevar mensajes de una dependencia a otra', 'Prefiere ser miembro de la Policía'),
(19, 'Le gusta sentirse importante sabiendo que de usted depende la rapidez o la lentitud de una solicitud', 'Prefiere trabajar en un bazar'),
(20, 'Le gustaría desarrollar técnicas de mayor eficiencia en el trámite documentario de un ministerio público', 'Prefiere escribir en otro idioma'),
(21, 'Le interesa trabajar en la implementación de bibliotecas distritales', 'Prefiere asumir la responsabilidad legal para que un fugitivo con residencia en otro país sea devuelto a su país');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ccco`
--

CREATE TABLE `ccco` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ccco`
--

INSERT INTO `ccco` (`id`, `a`, `b`) VALUES
(1, 'Te gusta conocer el funcionamiento de las máquinas impresoras de periódicos', 'Prefiere trabajar en el montaje fotográfico de un diario o revista'),
(2, 'Le gusta desarrollar programas de computación para proveer de información rápida y eficiente: a una empresa, institución, etc', 'Prefiere obtener fotografías que hagan noticia'),
(3, 'Le gusta mucho conocer el problema de las personas y tramitar su solución', 'Prefiere dedicar su tiempo a la búsqueda de personajes que hacen noticia'),
(4, 'Le gusta estudiar las características territoriales de los continentes', 'Prefiere entrevistar a políticos con el propósito de establecer su posición frente a un problema'),
(5, 'Le gusta proyectar el tipo de muebles, cortinas y adornos sea para una oficina o para un hogar', 'Prefiere trabajar como redactor en un diario o revista'),
(6, 'Le gusta redactar cartas comerciales, al igual que oficios y solicitudes', 'Prefiere averiguar lo que opina el público respecto a un producto'),
(7, 'Le gusta estudiar las leyes de la oferta y la demanda', 'Prefiere redactar el tema para un anuncio publicitario'),
(8, 'Le gusta organizar el servicio de inteligencia de un cuartel', 'Prefiere trabajar en una agencia de publicidad'),
(9, 'Le gusta trabajar buscando casas de alquiler para ofrecerlas al público', 'Prefiere estudiar las características psicológicas para lograr un buen impacto publicitario'),
(10, ' Le interesa investigar acerca de cómo se originaron los idiomas', 'Prefiere preparar y ejecutar encuestas para conocer la opinión de las personas'),
(11, 'Le agrada hacer los trámites legales de un juicio de divorcio', 'Prefiere trabajar estableciendo contactos entre una empresa y otra'),
(12, 'Le gusta escribir artículos deportivos para un diario', 'Prefiere determinar la resistencia de los materiales para una construcción'),
(13, 'Le gusta escribir artículos culturales para un diario', 'Prefiere pensar largamente acerca de la forma como el hombre podría mejorar su existencia'),
(14, 'Le gusta saber como se organiza una editorial periodística', 'Prefiere conocer las características de los órganos humanos y como funcionan'),
(15, 'Le gustaría dedicar su tiempo a la organización de eventos deportivos entre dos o mas centros laborales', 'Preferiría dedicarse al estudio de la vida y obra de los grandes actores del cine y del teatro'),
(16, ' Le gusta trabajar defendiendo el prestigio de su centro laboral', 'Prefiere trabajar recibiendo y entregando documentos valorados como: cheques, giros, libretas de ahorros, etc'),
(17, 'Le gustaría trabajar exclusivamente promocionando la imagen de su centro laboral', 'Prefiere estudiar las grandes corrientes ideológicas del mundo'),
(18, 'Le gusta la aventura cuando está dirigida a descubrir algo que haga noticia', 'Prefiere conocer el mecanismo de los aviones de guerra'),
(19, 'Le gustaría ganarse la vida escribiendo para un diario o revista', 'Prefiere estudiar el mercado y descubrir el producto de mayor demanda'),
(20, 'Le gustaría incorporarse al colegio de periodistas del Perú', 'Prefiere aprender otro idioma'),
(21, 'Le interesa investigar sobre los problemas del lenguaje en la comunicación masiva', 'Prefiere redactar documentos legales para contratos internacionales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ccep`
--

CREATE TABLE `ccep` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ccep`
--

INSERT INTO `ccep` (`id`, `a`, `b`) VALUES
(1, ' Te gusta investigar sobre las características de los regímenes totalitarios, democráticos, republicanos, etc', 'Prefiere ser el representante de su país en el extranjero'),
(2, 'Le gusta estudiar acerca de cómo la energía se transforma en movimiento', 'Preferiría hacer una tesis sobre manejo económico para el país'),
(3, 'Le agrada leer sobre la vida y obra de grandes personajes de educación, sean: profesores, filósofos, psicólogos', 'Prefiere estudiar acerca de las bases económicas de un país'),
(4, 'Le gusta estudiar los astros, sus características, origen y evolución', 'Prefiere establecer comparaciones entre los sistemas y modelos económicos del mundo'),
(5, 'Le gustaría trabajar exclusivamente promocionando la imagen de su centro laboral', 'Prefiere estudiar las grandes corrientes ideológicas del mundo'),
(6, 'Le gusta y practica el baile como expresión artística', 'Prefiere estudiar las bases de la organización política del Tahuantinsuyo'),
(7, 'Le gusta mucho saber sobre el manejo de los archivos públicos', 'Prefiere establecer diferencias entre los distintos modelos políticos'),
(8, 'Le gusta ser capitán de un buque de guerra', 'Le interesa más formar y conducir grupos con fines políticos'),
(9, 'Le agrada ser visitador médico', 'Prefiere dedicar su tiempo en la lectura de la vida y obra de los grandes politicos'),
(10, 'Siente placer buscando en el diccionario el significado de palabras nuevas', 'Prefiere dedicar todo su tiempo en aras de la paz entre las naciones'),
(11, 'Le interesa mucho estudiar el código penal', 'Prefiere estudiar los sistemas políticos de otros países'),
(12, 'Le agrada dedicar su tiempo en el estudio de teorías económicas', 'Prefiere dedicar su tiempo en la lectura de revistas sobre mecánica'),
(13, 'Le interesa mucho conocer los mecanismos de la economía nacional', 'Prefiere ser guía espiritual de las personas'),
(14, 'Le gusta proyectar los mecanismos de inversión económica de unaempresa', 'Prefiere analizar las tierras para obtener mayor producción agropecuaria'),
(15, 'Le gusta estudiar las leyes de la oferta y la demanda', 'Prefiere redactar el tema para un anuncio publicitario'),
(16, 'Le agrada mucho estudiar los fundamentos por los que una moneda se devalúa', 'Prefiere la lectura acerca de la vida y obra de grandes escultores como Miguel angel, Leonardo de Vinci, etc'),
(17, 'Le gusta dedicar su tiempo en el conocimiento del por qué ocurre la inflación económica', 'Prefiere dedicarse al estudio de cómo se organiza una biblioteca'),
(18, 'Le gustaría trabajar estableciendo vínculos culturales con otros países', 'Prefiere el trabajo en la detección y comprobación del delito'),
(19, 'Le gusta planificar sea para una empresa local o a nivel nacional', 'Prefiere el negocio de una bodega o tienda de abarrotes'),
(20, 'Le agradaría mucho ser secretario general de una central sindical', 'Prefiere dedicar su tiempo al estudio de lenguas extintas (muertas)'),
(21, 'Le agrada ser miembro activo de una agrupación política', 'Prefiere escuchar acusaciones y defensas para sancionar de acuerdo a lo que la ley señala');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ccfm`
--

CREATE TABLE `ccfm` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ccfm`
--

INSERT INTO `ccfm` (`id`, `a`, `b`) VALUES
(1, ' Te gusta resolver problemas de matemáticas', 'Prefiere diseñar el modelo de casas, edificios, parques, etc'),
(2, 'Le agrada observar la conducta de las personas y opinar sobre su personalidad', 'Prefiere expresar un fenómeno concreto en una ecuación matemática'),
(3, 'Le gusta caminar por los cerros buscando piedras raras', 'Prefiere diseñar viviendas de una Urbanización'),
(4, 'Le gusta escribir artículos deportivos para un diario', 'Prefiere determinar la resistencia de los materiales para una construcción'),
(5, 'Le gusta hacer tallado en madera', 'Prefiere calcular la cantidad de materiales para una construcción'),
(6, 'Le gusta ordenar y archivar documentos', 'Prefiere proyectar el sistema eléctrico para una construcción'),
(7, 'Le agrada dedicar su tiempo en el estudio de teorías económicas', 'Prefiere dedicar su tiempo en la lectura de revistas sobre mecánica'),
(8, 'Le gusta mucho la vida militar', 'Prefiere diseñar: máquinas, motores, etc, de alto rendimiento'),
(9, 'Le gusta estudiar acerca de cómo formar una cooperativa', 'Prefiere estudiar el lenguaje de computación IBM'),
(10, ' Le agrada estudiar la gramática', 'Prefiere estudiar las matemáticas'),
(11, 'Le interesa mucho ser abogado', 'Preferiría dedicarse a escribir un tratado de física-matemática'),
(12, 'Le interesa mucho estudiar como funciona un computador', 'Prefiere el estudio de las leyes y principios de la conducta psicológica'),
(13, 'Le gusta proyectar las redes de agua y desagüe de una ciudad', 'Prefiere estudiar acerca de las enfermedades de la dentadura'),
(14, 'Le gusta desarrollar programas de computación para proveer de información rápida y eficiente: a una empresa, institución, etc', 'Prefiere obtener fotografías que hagan noticia'),
(15, 'Le gusta trabajar haciendo instalaciones eléctricas', 'Prefiere dedicar su tiempo en la lectura de las novedades en la decoración de ambientes'),
(16, ' Prefiere estudiar acerca de cómo la energía se transforma en imágenes de radio, tv, etc', 'Le gusta tomar apuntes textuales o didácticos de otras personas'),
(17, 'Le gusta estudiar acerca de cómo la energía se transforma en movimiento', 'Preferiría hacer una tesis sobre manejo económico para el país'),
(18, 'Le gusta hacer los cálculos para el diseño de telas a gran escala', 'Le interesa más la mecánica de los barcos y submarinos'),
(19, 'Le gusta proyectar la extracción de metales de una mina', 'Prefiere estudiar el nombre de los medicamentos y su ventaja comercial'),
(20, 'Le gusta estudiar acerca de los reactores atómicos', 'Prefiere el estudio de las distintas formas literarias'),
(21, 'Le agrada estudiar la estructura atómica de los cuerpos', 'Prefiere asumir la defensa legal de una persona acusada por algún delito');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ccna`
--

CREATE TABLE `ccna` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ccna`
--

INSERT INTO `ccna` (`id`, `a`, `b`) VALUES
(1, 'Le gusta recolectar plantas y clasificarlas por especies', 'Prefiere leer sobre el origen y funcionamiento de las plantas y animales'),
(2, 'Le gusta proyectar las redes de agua y desagüe de una ciudad', 'Prefiere estudiar acerca de las enfermedades de la dentadura'),
(3, 'Le gusta visitar museos arqueológicos y conocer la vivienda y otros utensilios de nuestros antepasados', 'Prefiere hacer moldes para una dentadura postiza'),
(4, 'Le gusta saber como se organiza una editorial periodística', 'Prefiere conocer las características de los órganos humanos y como funcionan'),
(5, 'Le agrada construir; muebles, puertas, ventanas, etc', 'Prefiere estudiar acerca de las enfermedades de las personas'),
(6, 'Le agradaría trabajar en la recepción y trámite documentario de una oficinapública', 'Prefiere experimentar con las plantas para obtener nuevas especies'),
(7, 'Le gusta proyectar los mecanismos de inversión económica de unaempresa', 'Prefiere analizar las tierras para obtener mayor producción agropecuaria'),
(8, 'Le agrada recibir y ejecutar órdenes de un superior', 'Prefiere el estudio de los órganos de los animales y su funcionamiento'),
(9, 'Le gusta saber mucho sobre los principios económicos de una cooperativa', 'Prefiere conocer las enfermedades que aquejan, sea: el ganado, aves, perros, etc'),
(10, 'Le agrada estudiar los fenómenos (sonidos verbales) de su idioma, o de otros', 'Prefiere dedicar mucho de su tiempo en el estudio de la química'),
(11, 'Le agrada defender pleitos judiciales de recuperación de tierras', 'Prefiere hacer mezclas de sustancias químicas para obtener derivados con fines productivos'),
(12, 'Le gusta caminar por los cerros buscando piedras raras', 'Prefiere diseñar viviendas de una Urbanización'),
(13, 'Le gusta analizar las rocas, piedras, tierra para averiguar su composición mineral', 'Prefiere el estudio de las organizaciones sean: campesinas, educativas, laborales, económicas, políticas o religiosas'),
(14, 'Le gusta estudiar las características territoriales de los continentes', 'Prefiere entrevistar a políticos con el propósito de establecer su posición frente a un problema'),
(15, 'Le gusta estudiar los recursos geográficos', 'Prefiere observar el comportamiento de las personas e imitarlas'),
(16, 'Le gusta dedicar mucho de su tiempo en la lectura de la astronomía', 'prefiere trabajar clasificando los libros por autores'),
(17, 'Le gusta estudiar los astros, sus características, origen y evolución', 'Prefiere establecer comparaciones entre los sistemas y modelos económicos del mundo'),
(18, 'Le gustaría dedicar su tiempo en el descubrimiento de nuevos medicamentos', 'Prefiere dedicarse a la lectura acerca de la vida y obra de reconocidos militares, que han aportado en la organización de su institución'),
(19, 'Le agrada el estudio de los mecanismos de la visión y de sus enfermedades', 'Prefiere vender cosas'),
(20, 'Le gustaría escribir un tratado sobre anatomía humana', 'Prefiere recitar sus propios poemas'),
(21, 'Le gusta investigar de los recursos naturales de nuestro país (su fauna, su flora, su suelo)', 'Prefiere estudiar el derecho internacional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ccss`
--

CREATE TABLE `ccss` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ccss`
--

INSERT INTO `ccss` (`id`, `a`, `b`) VALUES
(1, ' Le interesa mucho estudiar como funciona un computador', 'Prefiere el estudio de las leyes y principios de la conducta psicológica'),
(2, 'Le agrada analizar la forma como se organiza un pueblo', 'Prefiere el estudio de las leyes y principios de la conducta psicológica'),
(3, 'Le gusta analizar las rocas, piedras, tierra para averiguar su composición mineral', 'Prefiere el estudio de las organizaciones sean: campesinas, educativas, laborales, económicas, políticas o religiosas'),
(4, 'Le gusta escribir artículos culturales para un diario', 'Prefiere pensar largamente acerca de la forma como el hombre podría mejorar su existencia'),
(5, 'Le agrada diseñar: muebles, puertas, ventanas, etc', 'Prefiere dedicar su tiempo a conocer las costumbres y tradiciones de los pueblos'),
(6, 'Le gusta mucho conocer el trámite documentario de un ministerio público', 'Prefieres el estudio de las religiones'),
(7, 'Le interesa mucho conocer los mecanismos de la economía nacional', 'Prefiere ser guía espiritual de las personas'),
(8, 'Le interesa mucho tener bajo su mando a un grupo de soldados', 'Prefiere enseñar lo que sabe a un grupo de compañeros'),
(9, 'Le gusta ser parte de la administración de una cooperativa', 'Prefiere el estudio de las formas más efectivas para la enseñanza de jóvenes y  niños'),
(10, 'Le interesa mucho estudiar la raíz gramatical de las palabras de su idioma', 'Prefiere dedicar su tiempo en la búsqueda de huacos y ruinas'),
(11, 'Le agrada mucho estudiar el código del derecho civil', 'Prefiere el estudio de las culturas peruanas y de otras naciones'),
(12, 'Le agrada observar la conducta de las personas y opinar sobre su personalidad', 'Prefiere expresar un fenómeno concreto en una ecuación matemática'),
(13, 'Le gusta visitar museos arqueológicos y conocer la vivienda y otros utensilios de nuestros antepasados', 'Prefiere hacer moldes para una dentadura postiza'),
(14, 'Le gusta mucho conocer el problema de las personas y tramitar su solución', 'Prefiere dedicar su tiempo a la búsqueda de personajes que hacen noticia'),
(15, 'Le agrada mucho visitar el hogar de los trabajadores con el fin de verificar su verdadera situación social y económica', 'Prefiere trabajar en el decorado de tiendas y vitrinas'),
(16, 'Le gusta leer sobre la vida y obra de los santos religiosos', 'Prefiere hacer catálogos o listados de los libros de una biblioteca'),
(17, 'Le agrada leer sobre la vida y obra de grandes personajes de educación, sean: profesores, filósofos, psicólogos', 'Prefiere estudiar acerca de las bases económicas de un país'),
(18, 'Le agrada observar y evaluar como se desarrolla la inteligencia y personalidad', 'Prefiere ser aviador'),
(19, 'Le gusta descifrar los diseños gráficos y escritos de culturas muy antiguas', 'Prefiere persuadir a la gente para que compre un producto'),
(20, 'Le agrada estudiar en torno de la problemática social del Perú', 'Prefiere escribir cuidando mucho ser comprendido al tiempo que sus escritos resulten agradables al lector'),
(21, 'Le gustaría escribir un tratado acerca de la historia del Perú', 'Prefiere asumir la defensa legal de un acusado por narcotráfico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fina`
--

CREATE TABLE `fina` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fina`
--

INSERT INTO `fina` (`id`, `a`, `b`) VALUES
(1, ' Le agrada llevar la contabilidad de una empresa o negocio', 'Prefiere hacer las planillas de pago para los trabajadores de una empresa o institución'),
(2, 'Le gusta proyectar la extracción de metales de una mina', 'Prefiere estudiar el nombre de los medicamentos y su ventaja comercial'),
(3, 'Le gusta descifrar los diseños gráficos y escritos de culturas muy antiguas', 'Prefiere persuadir a la gente para que compre un producto'),
(4, 'Le agrada el estudio de los mecanismos de la visión y de sus enfermedades', 'Prefiere vender cosas'),
(5, 'Le gustaría ganarse la vida escribiendo para un diario o revista', 'Prefiere estudiar el mercado y descubrir el producto de mayor demanda'),
(6, 'Le gusta actuar, representando a distintos personajes', 'Le agrada más tener su propio negocio'),
(7, 'Le gusta sentirse importante sabiendo que de usted depende la rapidez o la lentitud de una solicitud', 'Prefiere trabajar en un bazar'),
(8, 'Le gusta planificar sea para una empresa local o a nivel nacional', 'Prefiere el negocio de una bodega o tienda de abarrotes'),
(9, 'Le interesa mucho utilizar sus conocimientos en la construcción de armamentos', 'Prefiere organizar empresas de finanzas y comercio'),
(10, 'Le agrada escribir cartas y luego hacer tantas correcciones como sean necesarias', 'Prefiere ser incorporado como miembros de la corporación nacional de comercio'),
(11, 'Le gusta asumir la defensa legal de una persona acusada de asesinato', 'Prefiere ser incorporado como miembro de la corporación nacional de  comercio'),
(12, 'Le gusta estudiar acerca de cómo formar una cooperativa', 'Prefiere estudiar el lenguaje de computación IBM'),
(13, 'Le gusta ser parte de la administración de una cooperativa', 'Prefiere el estudio de las formas más efectivas para la enseñanza de jóvenes y  niños'),
(14, 'Le gusta saber mucho sobre los principios económicos de una cooperativa', 'Prefiere conocer las enfermedades que aquejan, sea: el ganado, aves, perros, etc'),
(15, 'Le gusta trabajar buscando casas de alquiler para ofrecerlas al público', 'Prefiere estudiar las características psicológicas para lograr un buen impacto publicitario'),
(16, 'Le gustaría trabajar tramitando la compra-venta de inmuebles', 'Prefiere utilizar las líneas y colores para expresar un sentimiento'),
(17, 'Le gusta trabajar tramitando la compra-venta de vehículos motorizados', 'Prefiere transcribir los documentos de la administración pública'),
(18, 'Le agrada ser visitador médico', 'Prefiere dedicar su tiempo en la lectura de la vida y obra de los grandes politicos'),
(19, 'Le gusta persuadir a los boticarios en la compra de nuevos medicamentos', 'Prefiere trabajar vigilando a los presos en las prisiones'),
(20, 'Le gusta llevar la estadística de ingresos y egresos mensuales de una empresa o tal vez de una nación', 'Prefiere los cursos de idiomas: Inglés, Francés, Italiano, etc'),
(21, 'Le gusta evaluar la producción laboral de un grupo de trabajadores', 'Prefiere plantear previa investigación la acusación de un sujeto que ha ido en contra de la ley');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `haa`
--

CREATE TABLE `haa` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `haa`
--

INSERT INTO `haa` (`id`, `a`, `b`) VALUES
(1, ' Te gusta trabajar custodiando el orden público', 'Prefiere ser vigilante receloso de nuestras fronteras'),
(2, 'Le gusta hacer los cálculos para el diseño de telas a gran escala', 'Le interesa más la mecánica de los barcos y submarinos'),
(3, 'Le agrada observar y evaluar como se desarrolla la inteligencia y personalidad', 'Prefiere ser aviador'),
(4, 'Le gustaría dedicar su tiempo en el descubrimiento de nuevos medicamentos', 'Prefiere dedicarse a la lectura acerca de la vida y obra de reconocidos militares, que han aportado en la organización de su institución'),
(5, 'Le gusta la aventura cuando está dirigida a descubrir algo que haga noticia', 'Prefiere conocer el mecanismo de los aviones de guerra'),
(6, 'Le gusta ser parte de una agrupación de baile y danzas', 'Preferiría pertenecer a la Fuerza Aérea'),
(7, 'Le gusta el trabajo de llevar mensajes de una dependencia a otra', 'Prefiere ser miembro de la Policía'),
(8, 'Le gustaría trabajar estableciendo vínculos culturales con otros países', 'Prefiere el trabajo en la detección y comprobación del delito'),
(9, 'Le gusta persuadir a los boticarios en la compra de nuevos medicamentos', 'Prefiere trabajar vigilando a los presos en las prisiones'),
(10, 'Le apasiona leer de escritores serios y famosos', 'Prefiere organizar el servicio de inteligencia en la destrucción del'),
(11, 'Le gusta asumir la defensa legal de una persona acusada de robo', 'Prefiere conocer el mecanismo de las armas de fuego'),
(12, 'Le gusta mucho la vida militar', 'Prefiere diseñar: máquinas, motores, etc, de alto rendimiento'),
(13, 'Le interesa mucho tener bajo su mando a un grupo de soldados', 'Prefiere enseñar lo que sabe a un grupo de compañeros'),
(14, 'Le agrada recibir y ejecutar órdenes de un superior', 'Prefiere el estudio de los órganos de los animales y su funcionamiento'),
(15, 'Le gusta organizar el servicio de inteligencia de un cuartel', 'Prefiere trabajar en una agencia de publicidad'),
(16, 'Le agrada mucho la vida del marinero', 'Prefiere combinar colores para expresar con naturalidad y belleza un paisaje'),
(17, 'Le interesa mucho el conocimiento de la organización de un buque de guerra', 'Prefiere dedicarse a la recepción y comunicación de mensajes sean verbales o por escrito'),
(18, 'Le gusta ser capitán de un buque de guerra', 'Le interesa más formar y conducir grupos con fines políticos'),
(19, 'Le interesa mucho utilizar sus conocimientos en la construcción de armamentos', 'Prefiere organizar empresas de finanzas y comercio'),
(20, 'Le gustaría dedicarse al estudio de normas de alta peligrosidad', 'Prefiere trabajar como traductor'),
(21, 'Le interesa mucho saber como se organiza un ejército', 'Prefiere participar como jurado en un juicio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juri`
--

CREATE TABLE `juri` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `juri`
--

INSERT INTO `juri` (`id`, `a`, `b`) VALUES
(1, ' gustaría dedicarse a la legalización de documentos (contratos, cartas, partidas, títulos, etc.)', 'Prefiere ser incorporado en una comisión para redactar un proyecto de ley'),
(2, 'Le agrada estudiar la estructura atómica de los cuerpos', 'Prefiere asumir la defensa legal de una persona acusada por algún delito'),
(3, 'Le gustaría escribir un tratado acerca de la historia del Perú', 'Prefiere asumir la defensa legal de un acusado por narcotráfico'),
(4, 'Le gusta investigar de los recursos naturales de nuestro país (su fauna, su flora, su suelo)', 'Prefiere estudiar el derecho internacional'),
(5, 'Le interesa investigar sobre los problemas del lenguaje en la comunicación masiva', 'Prefiere redactar documentos legales para contratos internacionales'),
(6, 'Le interesa diseñar y/o confeccionar artículos de cuero', 'Prefiere asumir la defensa legal en la demarcación de fronteras territoriales'),
(7, 'Le interesa trabajar en la implementación de bibliotecas distritales', 'Prefiere asumir la responsabilidad legal para que un fugitivo con residencia en otro país sea devuelto a su país'),
(8, 'Le agrada ser miembro activo de una agrupación política', 'Prefiere escuchar acusaciones y defensas para sancionar de acuerdo a lo que la ley señala'),
(9, 'Le interesa mucho saber como se organiza un ejército', 'Prefiere participar como jurado en un juicio'),
(10, 'Le gusta evaluar la producción laboral de un grupo de trabajadores', 'Prefiere plantear previa investigación la acusación de un sujeto que ha ido en contra de la ley'),
(11, 'Le gusta dedicar mucho de su tiempo en la escritura de poemas, cuentos', 'Prefiere sentirse importante al saber que de su defensa legal depende la libertad de una persona'),
(12, 'Le interesa mucho ser abogado', 'Preferiría dedicarse a escribir un tratado de física-matemática'),
(13, 'Le agrada mucho estudiar el código del derecho civil', 'Prefiere el estudio de las culturas peruanas y de otras naciones'),
(14, 'Le agrada defender pleitos judiciales de recuperación de tierras', 'Prefiere hacer mezclas de sustancias químicas para obtener derivados con fines productivos'),
(15, 'Le agrada hacer los trámites legales de un juicio de divorcio', 'Prefiere trabajar estableciendo contactos entre una empresa y otra'),
(16, 'Le agrada tramitar judicialmente el reconocimiento de sus hijo', 'Le agrada más aprender a tocar algún instrumento musical'),
(17, 'Le gusta dedicar su tiempo planteando la defensa de un juicio de alquiler', 'Prefiere asesorar y aconsejar en torno a tramites documentarios'),
(18, 'Le interesa mucho estudiar el código penal', 'Prefiere estudiar los sistemas políticos de otros países'),
(19, 'Le gusta asumir la defensa legal de una persona acusada de robo', 'Prefiere conocer el mecanismo de las armas de fuego'),
(20, 'Le gusta asumir la defensa legal de una persona acusada de asesinato', 'Prefiere ser incorporado como miembro de la corporación nacional de  comercio'),
(21, 'Le interesaría ser el asesor legal de un ministro de estado', 'Prefiere aquellas situaciones que le inspiran a escribir');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ling`
--

CREATE TABLE `ling` (
  `id` int(11) NOT NULL,
  `a` varchar(400) DEFAULT NULL,
  `b` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ling`
--

INSERT INTO `ling` (`id`, `a`, `b`) VALUES
(1, ' gustaría ser incorporado como miembro de la Real Academia de la Lengua Española', 'Prefiere ser incorporado al Instituto Nacional del Idioma'),
(2, 'Le gusta estudiar acerca de los reactores atómicos', 'Prefiere el estudio de las distintas formas literarias'),
(3, 'Le agrada estudiar en torno de la problemática social del Perú', 'Prefiere escribir cuidando mucho ser comprendido al tiempo que sus escritos resulten agradables al lector'),
(4, 'Le gustaría escribir un tratado sobre anatomía humana', 'Prefiere recitar sus propios poemas'),
(5, 'Le gustaría incorporarse al colegio de periodistas del Perú', 'Prefiere aprender otro idioma'),
(6, 'Le gusta diseñar y/o confeccionar: adornos, utensilios, etc., en cerámica, vidrio, etc', 'Prefiere traducir textos escritos en otros idiomas'),
(7, 'Le gustaría desarrollar técnicas de mayor eficiencia en el trámite documentario de un ministerio público', 'Prefiere escribir en otro idioma'),
(8, 'Le agradaría mucho ser secretario general de una central sindical', 'Prefiere dedicar su tiempo al estudio de lenguas extintas (muertas)'),
(9, 'Le gustaría dedicarse al estudio de normas de alta peligrosidad', 'Prefiere trabajar como traductor'),
(10, 'Le gusta llevar la estadística de ingresos y egresos mensuales de una empresa o tal vez de una nación', 'Prefiere los cursos de idiomas: Inglés, Francés, Italiano, etc'),
(11, ' Le agrada estudiar la gramática', 'Prefiere estudiar las matemáticas'),
(12, 'Le interesa mucho estudiar la raíz gramatical de las palabras de su idioma', 'Prefiere dedicar su tiempo en la búsqueda de huacos y ruinas'),
(13, 'Le agrada estudiar los fenómenos (sonidos verbales) de su idioma, o de otros', 'Prefiere dedicar mucho de su tiempo en el estudio de la química'),
(14, ' Le interesa investigar acerca de cómo se originaron los idiomas', 'Prefiere preparar y ejecutar encuestas para conocer la opinión de las personas'),
(15, 'Le gusta estudiar las lenguas y dialectos aborígenes', 'Prefiere combinar sonidos para obtener una nueva melodía'),
(16, 'Le gusta dedicar gran parte de su tiempo al estudio de las normas y reglas para el uso adecuado del lenguaje', 'Prefiere trabajar como secretario adjunto al jefe'),
(17, 'Siente placer buscando en el diccionario el significado de palabras nuevas', 'Prefiere dedicar todo su tiempo en aras de la paz entre las naciones'),
(18, 'Le apasiona leer de escritores serios y famosos', 'Prefiere organizar el servicio de inteligencia en la destrucción del narcotráfico'),
(19, 'Le agrada escribir cartas y luego hacer tantas correcciones como sean necesarias', 'Prefiere ser incorporado como miembros de la corporación nacional de comercio'),
(20, 'Le gustaría ser incorporado como miembro de la Real Academia de la Lengua Española', 'Prefiere ser incorporado al Instituto Nacional del Idioma'),
(21, 'Le gusta dedicar mucho de su tiempo en la escritura de poemas, cuentos', 'Prefiere sentirse importante al saber que de su defensa legal depende la libertad de una persona');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `arte`
--
ALTER TABLE `arte`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `base_conocimiento`
--
ALTER TABLE `base_conocimiento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `base_hechos`
--
ALTER TABLE `base_hechos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `buro`
--
ALTER TABLE `buro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ccco`
--
ALTER TABLE `ccco`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ccep`
--
ALTER TABLE `ccep`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ccfm`
--
ALTER TABLE `ccfm`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ccna`
--
ALTER TABLE `ccna`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ccss`
--
ALTER TABLE `ccss`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fina`
--
ALTER TABLE `fina`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `haa`
--
ALTER TABLE `haa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `juri`
--
ALTER TABLE `juri`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ling`
--
ALTER TABLE `ling`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `arte`
--
ALTER TABLE `arte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `base_conocimiento`
--
ALTER TABLE `base_conocimiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT de la tabla `base_hechos`
--
ALTER TABLE `base_hechos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `buro`
--
ALTER TABLE `buro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `ccco`
--
ALTER TABLE `ccco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `ccep`
--
ALTER TABLE `ccep`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `ccfm`
--
ALTER TABLE `ccfm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `ccna`
--
ALTER TABLE `ccna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `ccss`
--
ALTER TABLE `ccss`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `fina`
--
ALTER TABLE `fina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `haa`
--
ALTER TABLE `haa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `juri`
--
ALTER TABLE `juri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `ling`
--
ALTER TABLE `ling`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
