<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tienda', function (Blueprint $table) {
            $table->increments('id');
            $table->string('direccion');        
            $table->string('telefono');
            $table->string('serie')->index();
            $table->integer('empresa')->index()->unsigned();
            $table->string('departamento');
            $table->string('provincia');
            $table->string('distrito');
            $table->string('codigo_ubigeo');
            $table->string('ubicacion_zona');
            $table->string('nombre_comercial');
            $table->tinyInteger('estado');        
            $table->foreign('empresa')->references('id')->on('empresa')->onDElete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tienda');
    }
}
