<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razon_social');
            $table->string('telefono');
            $table->string('departamento');            
            $table->string('provincia');
            $table->string('codigo_pais');
            $table->string('domicilio_fiscal');
            $table->string('distrito');
            $table->tinyInteger('cliente');
            $table->tinyInteger('proveedor');
            $table->string('email');
            $table->string('ruc');
            $table->string('dni');
            $table->integer('empresa')->index()->unsigned();
            $table->foreign('empresa')->references('id')->on('empresa')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacto');
    }
}
