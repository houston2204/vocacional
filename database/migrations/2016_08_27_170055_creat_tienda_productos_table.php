<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatTiendaProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tienda_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tienda')->index()->unsigned();
            $table->integer('producto')->index()->unsigned();
            $table->integer('cantidad');
            $table->foreign('tienda')->references('id')->on('tienda')->onDelete('cascade');
            $table->foreign('producto')->references('id')->on('producto')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tienda_productos');
    }
}
