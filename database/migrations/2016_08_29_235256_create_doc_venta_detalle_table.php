<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocVentaDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_venta_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doc_venta')->index()->unsigned();
            $table->integer('producto')->index()->unsigned();
            $table->integer('cantidad');
            $table->double('precio_venta_unitario');
            $table->string('unidad_medida');
            $table->string('descripcion');
            $table->double('valor_unitario');
            $table->double('igv');
            $table->double('valor_venta');
            $table->double('descuento');
            $table->string('afectacion_igv');
            $table->double('valor_referencial');
            $table->integer('numero');
            $table->integer('codigo');
            $table->string('')
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doc_venta_detalle');
    }
}
