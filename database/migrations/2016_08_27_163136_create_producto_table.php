<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('descripcion');
            $table->double('precio');
            $table->string('unidad');            
            $table->integer('categoria')->index()->unsigned();
            $table->integer('empresa')->index()->unsigned();
            $table->integer('marca')->index()->unsigned();              
            $table->foreign('categoria')->references('id')->on('categoria')->onDelete('cascade');
            $table->foreign('empresa')->references('id')->on('empresa')->onDElete('cascade');
            $table->foreign('marca')->references('id')->on('marca')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('producto');
    }
}
