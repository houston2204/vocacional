<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_compra', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_moneda');          
            $table->date('fecha_emision');
            $table->double('total_venta_og');
            $table->double('total_igv');
            $table->string('tipo_documento');
            $table->double('total_venta_oi');
            $table->double('total_venta_oe');
            $table->double('total_otros_cargos');
            $table->double('total_descuentos');
            $table->double('importe_venta_total');
            $table->double('descuento_global');
            $table->double('total_venta_ot');
            $table->string('numero');
            $table->date('fecha_envio');
            $table->string('estado_sunat');
            $table->tinyInteger('tipo');
            $table->integer('proveedor')->index()->unsigned();
            $table->integer('cliente')->index()->unsigned();
            $table->string('serie');
            $table->tinyInteger('estado');
            $table->foreign('proveedor')->references('id')->on('contacto')->onDelete('cascade');
            $table->foreign('cliente')->references('id')->on('tienda')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doc_compra');
    }
}
