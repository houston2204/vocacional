<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Faker\Provider\es_ES\Person;
class UserTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Faker = Faker::create();        
        for ($i=0; $i <50 ; $i++) { 
        
        \DB::table('users')->insert(array(
	    		'name'=>$Faker->name,
	    	   	'email'=>$Faker->freeEmail,
	    	   	'password'=>bcrypt('secret'),
	    	   	'dni'=>$Faker->numerify('########')	,    	   	
	    	   	'telefono'=>$Faker->tollFreePhoneNumber,	    	   	
	    	   	'direccion'=>$Faker->address,  	    	   	
	    	   	'nivel'=>$Faker->numberBetween($min = 1, $max = 4) ,                    
	    	   	'created_at'=>$Faker->dateTimeThisMonth($max = 'now'),
	    	   	'updated_at'=>$Faker->dateTimeThisMonth($max = 'now')
	    		));
        }

    }
}